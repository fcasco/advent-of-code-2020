Day 21: Allergen Assessment
###########################


Test 0
======

Rules
-----

mxmxvkd kfcds sqjhc nhms (contains dairy, fish)

trh fvjkl sbzzf mxmxvkd (contains dairy)

sqjhc fvjkl (contains soy)

sqjhc mxmxvkd sbzzf (contains fish)


Analysis
--------

D
F
S

m k s n t f b

food 1:
    D: m
    F: k
    S: ?

food 2:
    D: m
    F: k =
    S: ? =

food 3:
    D: m =
    F: k =
    S: s +

food 4:
    D: m
    F: k > b
    S: s


1. m k s n (contains dairy, fish): mD kF s n, mD k sF n, mD k s nF, mF sD, k n, m sD kF n, m sD k nF
   D F - -
   D - F -
   D - - F
   F D - -
   - D F -
   - D - F
   F - D -
   - F D -
   - - D F
   F - - D
   - F - D
   - - F D

2. t f b m (contains dairy)

3. s f (contains soy)

4. s m b (contains fish)

dairy: m k s n, t f b m
fish: m k s n, s m b
soy: s f


(
    {
        'dairy': {'trh', 'kfcds', 'nhms', 'sbzzf', 'sqjhc', 'fvjkl', 'mxmxvkd'},
        'fish': {'sqjhc', 'kfcds', 'nhms', 'mxmxvkd', 'sbzzf'},
        'soy': {'sqjhc', 'fvjkl'}
    },
    {
        'fvjkl': {'dairy', 'soy'},
            'kfcds': {'dairy', 'fish'},
        'mxmxvkd': {'dairy', 'fish'},
            'nhms': {'dairy', 'fish'},
            'sbzzf': {'dairy', 'fish'}
        'sqjhc': {'dairy', 'fish', 'soy'},
            'trh': {'dairy'},
    }
)

['kfcds', 'nhms', 'sbzzf', 'trh']
