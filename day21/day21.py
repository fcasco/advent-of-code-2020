import argparse
import collections
import functools
import itertools
import re


def parse_foods_list(data):
    """
        >>> foods = parse_foods_list([
        ... 'mxmxvkd kfcds sqjhc nhms (contains dairy, fish)',
        ... 'trh fvjkl sbzzf mxmxvkd (contains dairy)',
        ... 'sqjhc fvjkl (contains soy)',
        ... 'sqjhc mxmxvkd sbzzf (contains fish)',
        ... ])
        >>> len(foods)
        4
        >>> sorted(sorted(foods, key=lambda x: len(x['ingredients']))[0]['ingredients'])
        ['fvjkl', 'sqjhc']
        >>> sorted(sorted(foods, key=lambda x: len(x['ingredients']))[0]['allergenes'])
        ['soy']
    """
    foods = []
    line_re = re.compile(r'(.*) \(contains (.*)\)')

    for line in data:
        match = line_re.search(line)
        if not match:
            print(f'{line} does not match')
            continue

        foods.append({
            'ingredients': set(match.groups()[0].split()),
            'allergenes': set(match.groups()[1].split(', ')),
        })

    return foods


def validate_assignment(allergenes_ingredients, foods):
    all_assigned = all(x is not None for x in allergenes_ingredients.values())
    if not all_assigned:
        # print('not all assigned')
        return False

    for food in foods:
        if any(allergenes_ingredients[x] not in food['ingredients']
               for x in food['allergenes']):
            # print(f'some allergene not in the food {food}')
            return False

    if any(x > 1 for x in collections.Counter(allergenes_ingredients.values()).values()):
        # print(f'some ingredient has more than one allergene')
        return False

    return True


def get_allergenes_ingredients(foods):
    """
        >>> allergenes_ingredients = get_allergenes_ingredients([
        ... {'ingredients': {'mxmxvkd', 'kfcds', 'sqjhc', 'nhms'},
        ...  'allergenes': {'dairy', 'fish'}},
        ... {'ingredients': {'trh', 'fvjkl', 'sbzzf', 'mxmxvkd'},
        ...  'allergenes': {'dairy'}},
        ... {'ingredients': {'sqjhc', 'fvjkl'},
        ...  'allergenes': {'soy'}},
        ... {'ingredients': {'sqjhc', 'mxmxvkd', 'sbzzf'},
        ...  'allergenes': {'fish'}},
        ... ])
        >>> sorted(allergenes_ingredients.items())
        [('dairy', 'mxmxvkd'), ('fish', 'sqjhc'), ('soy', 'fvjkl')]
    """
    allergenes = functools.reduce(lambda x, y: x | y, [x['allergenes'] for x in foods])

    foods.sort(key=lambda x: len(x['ingredients']))

    possible_assignments = {}
    for allergene in allergenes:
        possible_ingredients = [x['ingredients'] for x in foods if allergene in x['allergenes']]
        possible_assignments[allergene] = list(functools.reduce(lambda x, y: x & y,
                                                                possible_ingredients))

    all_offsets = itertools.product(*[range(len(x)) for x in possible_assignments.values()])

    allergenes_ingredients = {x: None for x in allergenes}
    while not validate_assignment(allergenes_ingredients, foods):

        offsets = next(all_offsets)
        for i, allergene in enumerate(allergenes):
            allergenes_ingredients[allergene] = possible_assignments[allergene][offsets[i]]

    return allergenes_ingredients


def main(args):
    with open(args.input_file) as input_data:
        data = [x.strip() for x in input_data.readlines()]

    foods = parse_foods_list(data)
    ingredients = functools.reduce(lambda x, y: x | y, [x['ingredients'] for x in foods])

    allergenes_ingredients = get_allergenes_ingredients(foods)
    safe_ingredients = [x for x in ingredients if x not in allergenes_ingredients.values()]
    sum_of_safe_ingredients = sum(len([x for x in foods if y in x['ingredients']])
                                  for y in safe_ingredients)
    print(f'The safe ingredients are {safe_ingredients}')
    print(f'The sum of their foods is {sum_of_safe_ingredients}')

    ingredients_list = ','.join([x[1] for x in sorted(allergenes_ingredients.items(),
                                                      key=lambda x: x[0])])
    print(f'The cannonical dangerous ingredients list is {ingredients_list}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 21: Allergen Assessment')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
