import argparse
import functools
import re


def split_data(data):
    """
        >>> split_data('1 + (2 * 3) + ((4 * 5))')
        [1, '+', '(', 2, '*', 3, ')', '+', '(', '(', 4, '*', 5, ')', ')']
    """
    return [int(x) if x.isdigit() else x for x in data.replace('(', '( ')
                                                      .replace(')', ' )')
                                                      .split(' ')]


def solve(expression):
    """
        >>> solve('1 + 2 * 3 + 4 * 5 + 6')
        71

        >>> solve('1 + (2 * 3) + (4 * (5 + 6))')
        51

        >>> solve('5 + (8 * 3 + 9 + 3 * 4 * 3)')
        437

        >>> solve('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))')
        12240

        >>> solve('((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2')
        13632
    """
    operations = {
            '+': lambda x, y: x + y,
            '*': lambda x, y: x * y,
    }

    sub_expression_re = re.compile(r'\([^\(\)]+\)')

    expression = f'({expression})'

    while '(' in expression:
        try:
            sub_expression = next(sub_expression_re.finditer(expression))
        except StopIteration:
            break

        operands = []
        operation = None
        for part in sub_expression.group().strip('()').split():
            if part in operations:
                operation = operations.get(part, None)
            else:
                operands.append(int(part))

                if len(operands) == 2:
                    operands = [operation(*operands)]

        expression = f'{expression[:sub_expression.start()]}'\
            + f'{operands[0]}' \
            + f'{expression[sub_expression.end():]}'

    return operands[0]


def solve_sum_first(expression):
    """
        >>> solve_sum_first('1 + (2 * 3) + (4 * (5 + 6))')
        51

        >>> solve_sum_first('2 * 3 + (4 * 5)')
        46

        >>> solve_sum_first('5 + (8 * 3 + 9 + 3 * 4 * 3)')
        1445

        >>> solve_sum_first('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))')
        669060
    """
    sub_expression_re = re.compile(r'\([^\(\)]+\)')

    expression = f'({expression})'

    while '(' in expression:
        try:
            sub_expression = next(sub_expression_re.finditer(expression))
        except StopIteration:
            break

        sub_expression_string = sub_expression.group().strip('()')
        numbers = [[int(x) for x in factors.split('+')]
                   for factors in sub_expression_string.split('*')]
        sumed = [functools.reduce(lambda x, y: x + y, operands)
                 for operands in numbers]
        result = functools.reduce(lambda x, y: x * y, sumed)

        expression = f'{expression[:sub_expression.start()]}'\
            + f'{result}' \
            + f'{expression[sub_expression.end():]}'

    return result


def main(args):
    with open(args.input_file) as input_data:
        data = [x.strip() for x in input_data.readlines()]

    sum_of_all_1 = sum(solve(x) for x in data)
    print(f'The sum of the result of all the operations for part I is {sum_of_all_1}')

    sum_of_all_2 = sum(solve_sum_first(x) for x in data)
    print(f'The sum of the result of all the operations for part II is {sum_of_all_2}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 0: Template')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
