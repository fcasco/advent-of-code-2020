import argparse
import functools


def get_jolts_diffs(adapters):
    """
        >>> get_jolts_diffs([
        ... 16,
        ... 10,
        ... 15,
        ... 5,
        ... 1,
        ... 11,
        ... 7,
        ... 19,
        ... 6,
        ... 12,
        ... 4,
        ... ])
        {1: 7, 3: 5}

        >>> get_jolts_diffs([
        ... 28,
        ... 33,
        ... 18,
        ... 42,
        ... 31,
        ... 14,
        ... 46,
        ... 20,
        ... 48,
        ... 47,
        ... 24,
        ... 23,
        ... 49,
        ... 45,
        ... 19,
        ... 38,
        ... 39,
        ... 11,
        ... 1,
        ... 32,
        ... 25,
        ... 35,
        ... 8,
        ... 17,
        ... 7,
        ... 9,
        ... 4,
        ... 2,
        ... 34,
        ... 10,
        ... 3,
        ... ])
        {1: 22, 3: 10}
    """
    adapters.sort()

    diffs = {adapters[0]: 1, 3: 1}
    adapters_len = len(adapters)
    for i in range(1, adapters_len):
        diff = adapters[i] - adapters[i - 1]
        diffs.setdefault(diff, 0)
        diffs[diff] += 1

    return diffs


@functools.lru_cache(maxsize=None)
def count_combinations(adapters, target=None, source=0):
    """
        >>> count_combinations((1, 2))
        2

        >>> count_combinations((1, 2, 3))
        4

        >>> count_combinations((16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4))
        8

        >>> count_combinations((1, 4, 5, 6, 7, 10, 11, 12, 15, 16, 19, 20, 21))
        16

        >>> count_combinations((10, 6, 4, 7, 1, 5))
        4

        >>> count_combinations((4, 11, 7, 8, 1, 6, 5))
        7

        >>> count_combinations((17, 6, 10, 5, 13, 7, 1, 4, 12, 11, 14))
        28

        >>> count_combinations((3, 1, 6, 2))
        4

        >>> count_combinations((
        ... 28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39,
        ... 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3,
        ... ))
        19208
    """
    # ugly hack because this is just for fun
    if 0 not in adapters:
        adapters = adapters + (0, )

    if target is None:
        target = max(adapters) + 3

    prev_adapters = [x for x in adapters if x < target and target - x <= 3]
    combinations = 1
    if prev_adapters:
        combinations = sum(count_combinations(adapters, x) for x in prev_adapters)

    return combinations


def get_adapters_combinations(adapters, target=None, cache=None):
    """
    """
    """
        >>> get_adapters_combinations([16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4], 22)
        8

        >>> get_adapters_combinations([
        ... 28,
        ... 33,
        ... 18,
        ... 42,
        ... 31,
        ... 14,
        ... 46,
        ... 20,
        ... 48,
        ... 47,
        ... 24,
        ... 23,
        ... 49,
        ... 45,
        ... 19,
        ... 38,
        ... 39,
        ... 11,
        ... 1,
        ... 32,
        ... 25,
        ... 35,
        ... 8,
        ... 17,
        ... 7,
        ... 9,
        ... 4,
        ... 2,
        ... 34,
        ... 10,
        ... 3,
        ... ])
        19208
    """
    if cache is None:
        cache = {}

    if target is None:
        target = max(adapters)

    if target < 3:
        return 1

    combinations = 0
    prev_adapters = [x for x in adapters if 0 < target - x <= 3]
    for prev_adapter in prev_adapters:
        if prev_adapter < 3:
            combinations = 1
        if prev_adapter in cache:
            combinations += cache[prev_adapter]
        else:
            combinations += get_adapters_combinations(adapters, prev_adapter, cache)

    return combinations


def main(args):
    with open(args.input_file) as input_data:
        adapters = list(map(int, input_data.readlines()))

    diffs = get_jolts_diffs(adapters)
    product = functools.reduce(lambda x, y: x * y, diffs.values())

    print(f'Adapters diffs: {diffs}, product {product}')

    # this is an ugly hack to include all the possible combinations that reach 0
    print(f'Combinations: {count_combinations(tuple(adapters + [0]))}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 10: Adapter Array')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
