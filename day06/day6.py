import argparse


def get_answers_sum_1(answers):
    """
        >>> get_answers_sum_1(())
        0

        >>> get_answers_sum_1((
        ... 'abc',
        ... '',
        ... 'a',
        ... 'b',
        ... 'c',
        ... '',
        ... 'ab',
        ... 'ac',
        ... '',
        ... 'a',
        ... 'a',
        ... 'a',
        ... 'a',
        ... '',
        ... 'b',
        ... ))
        11
    """
    total_answers = 0
    group_answers = set()
    for passenger_answers in answers:
        if passenger_answers:
            group_answers |= set(passenger_answers)
        else:
            total_answers += len(group_answers)
            group_answers = set()

    if len(group_answers) > 0:
        total_answers += len(group_answers)

    return total_answers


def get_answers_sum_2(answers):
    """
        >>> get_answers_sum_2(())
        0

        >>> get_answers_sum_2((
        ... 'a',
        ... 'b',
        ... 'c',
        ... ))
        0

        >>> get_answers_sum_2((
        ... 'abc',
        ... '',
        ... 'a',
        ... 'b',
        ... 'c',
        ... '',
        ... 'ab',
        ... 'ac',
        ... '',
        ... 'a',
        ... 'a',
        ... 'a',
        ... 'a',
        ... '',
        ... 'b',
        ... ))
        6
    """
    total_answers = 0
    group_answers = set()
    in_group = False
    for i, passenger_answers in enumerate(answers):
        if passenger_answers:
            if not in_group:
                group_answers = set(passenger_answers)
                in_group = True
            else:
                group_answers &= set(passenger_answers)

        else:
            total_answers += len(group_answers)
            in_group = False

    if len(group_answers) > 0:
        total_answers += len(group_answers)

    return total_answers


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        answers = [x.strip() for x in input_data.readlines()]

    answers_sum_1 = get_answers_sum_1(answers)
    answers_sum_2 = get_answers_sum_2(answers)

    print(f'Answers sum (part 1): {answers_sum_1}')
    print(f'Answers sum (part 2): {answers_sum_2}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 6: Custom Customs')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
