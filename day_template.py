import argparse


def solve(data):
    pass


def main(args):
    with open(args.input_file) as input_data:
        data = list(map(int, input_data.readlines()))

    solve(data)


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 0: Template')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
