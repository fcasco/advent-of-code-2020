import argparse
import re


def count_valid_passports(passports_data):
    """
        >>> count_valid_passports((
        ... 'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd',
        ... 'byr:1937 iyr:2017 cid:147 hgt:183cm',
        ... ))
        1

        >>> count_valid_passports((
        ... 'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd',
        ... ))
        0

        >>> count_valid_passports((
        ... 'ecl:gry pid:860033327 eyr:2020 hcl:#fffffd',
        ... 'byr:1937 iyr:2017 cid:147 hgt:183cm',
        ... '',
        ... 'iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884',
        ... 'hcl:#cfa07d byr:1929',
        ... '',
        ... 'hcl:#ae17e1 iyr:2013',
        ... 'eyr:2024',
        ... 'ecl:brn pid:760753108 byr:1931',
        ... 'hgt:179cm',
        ... '',
        ... 'hcl:#cfa07d eyr:2025 pid:166559648',
        ... 'iyr:2011 ecl:brn hgt:59in',
        ... ))
        2

        >>> count_valid_passports((
        ... 'eyr:1972 cid:100',
        ... 'hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926',
        ... '',
        ... 'iyr:2019',
        ... 'hcl:#602927 eyr:1967 hgt:170cm',
        ... 'ecl:grn pid:012533040 byr:1946',
        ... '',
        ... 'hcl:dab227 iyr:2012',
        ... 'ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277',
        ... '',
        ... 'hgt:59cm ecl:zzz',
        ... 'eyr:2038 hcl:74454a iyr:2023',
        ... 'pid:3556412378 byr:2007',
        ... ))
        0

        >>> count_valid_passports((
        ... 'pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980',
        ... 'hcl:#623a2f',
        ... '',
        ... 'eyr:2029 ecl:blu cid:129 byr:1989',
        ... 'iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm',
        ... '',
        ... 'hcl:#888785',
        ... 'hgt:164cm byr:2001 iyr:2015 cid:88',
        ... 'pid:545766238 ecl:hzl',
        ... 'eyr:2022',
        ... '',
        ... 'iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719',
        ... ))
        4
    """
    required_fields = set(('byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'))

    passport_validations = (
            # byr (Birth Year) - four digits; at least 1920 and at most 2002.
            lambda x: x['byr'].isdigit() and 1920 <= int(x['byr']) <= 2002,

            # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
            lambda x: x['iyr'].isdigit() and 2010 <= int(x['iyr']) <= 2020,

            # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
            lambda x: x['eyr'].isdigit() and 2020 <= int(x['eyr']) <= 2030,

            # hgt (Height) - a number followed by either cm or in:
            # If cm, the number must be at least 150 and at most 193.
            # If in, the number must be at least 59 and at most 76.
            lambda x: (
                (
                    x['hgt'].endswith('cm') \
                    and x['hgt'].strip('cm').isdigit() \
                    and 150 <= int(x['hgt'].strip('cm')) <= 193
                ) or (
                    x['hgt'].endswith('in') \
                    and x['hgt'].strip('in').isdigit() \
                    and 59 <= int(x['hgt'].strip('in')) <= 76
                )),

            # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
            lambda x: re.match('^#[a-f0-9]{6}$', x['hcl']),

            # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
            lambda x: x['ecl'] in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'),

            # pid (Passport ID) - a nine-digit number, including leading zeroes.
            # lambda x: x['pid'].isdigit() and len(x['pid']) == 9,
            lambda x: re.match(r'^\d{9}$', x['pid']),
    )

    passport_data = {}
    valid_passports_count = 0
    last_line = len(passports_data) - 1
    for i, data_line in enumerate(passports_data):
        if data_line.strip():
            passport_fields = {k: v
                               for k, v in (x.split(':') for x in data_line.strip().split(' '))}
            passport_data.update(passport_fields)

        if not data_line.strip() or i == last_line:
            included_fields = set(passport_data.keys())
            missing_fields = required_fields - included_fields

            if not missing_fields and all(x(passport_data) for x in passport_validations):
                valid_passports_count += 1

            passport_data = {}

    return valid_passports_count


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        passports_data = input_data.readlines()

        valid_passports_count = count_valid_passports(passports_data)

    print(f'Valid passports count: {valid_passports_count}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 4: Passport Processing')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
