import argparse


def parse_rule(rule):
    """
        >>> parse_rule(
        ... 'drab plum bags contain 5 clear turquoise bags, '
        ... '5 striped aqua bags, '
        ... '4 dotted gold bags.'
        ... )
        ('drab plum', [('clear turquoise', 5), ('striped aqua', 5), ('dotted gold', 4)])

        >>> parse_rule('shiny brown bags contain 4 dark maroon bags.')
        ('shiny brown', [('dark maroon', 4)])

        >>> parse_rule('posh lavender bags contain no other bags.')
        ('posh lavender', [])

        >>> parse_rule('bright white bags contain 1 shiny gold bag.')
        ('bright white', [('shiny gold', 1)])
    """
    source_bag, target_bags_raw = rule.split(' contain ')

    target_bags = []
    for target_bag_raw in target_bags_raw.strip().split(','):
        if target_bag_raw == 'no other bags.':
            continue
        quantity, color = target_bag_raw.strip(' .').split(' ', 1)
        target_bags.append((color.replace('bags', '').replace('bag', '').strip(),
                            int(quantity)))

    return source_bag.replace('bags', '').strip(), target_bags


def parse_rules(rules):
    """
        >>> parse_rules((
        ... 'plaid purple bags contain 2 posh black bags.',
        ... 'shiny brown bags contain 4 dark maroon bags.',
        ... ))
        {'plaid purple': [('posh black', 2)], 'shiny brown': [('dark maroon', 4)]}

        >>> parse_rules((
        ... 'plaid silver bags contain 2 plaid tomato bags, 3 dim indigo bags.',
        ... 'plaid silver bags contain 2 dark maroon bags.',
        ... ))
        {'plaid silver': [('plaid tomato', 2), ('dim indigo', 3), ('dark maroon', 2)]}
    """
    rules_tree = {}

    for rule in rules:
        source_bag, target_bags = parse_rule(rule)
        rules_tree.setdefault(source_bag, []).extend(target_bags)

    return rules_tree


def get_outer_bags_for(color, rules_tree):
    """
        >>> get_outer_bags_for('r', {'b': [('r', 1)], 'g': [('o', 2), ('y', 3)]})
        {'b'}

        >>> sorted(get_outer_bags_for('r', {'b': [('r', 1)], 'g': [('o', 2), ('b', 3)]}))
        ['b', 'g']

        >>> sorted(get_outer_bags_for('r',
        ... {
        ... 'b': [('r', 1)],
        ... 'g': [('b', 3)],
        ... 'y': [('g', 2)],
        ... 'v': [('t', 2)],
        ... }))
        ['b', 'g', 'y']
    """
    direct_containers = set(k for k, v in rules_tree.items() if any(color in x for x in v))
    indirect_containers = (get_outer_bags_for(x, rules_tree) for x in direct_containers)

    return set.union(direct_containers, *indirect_containers)


def get_inner_bags_for(color, rules_tree):
    """
        >>> get_inner_bags_for('b', {'b': [('r', 3)]})
        ['r', 'r', 'r']

        >>> get_inner_bags_for('b', {'b': [('r', 1), ('g', 2)]})
        ['r', 'g', 'g']

        >>> get_inner_bags_for('b', {'b': [('r', 1), ('g', 2)], 'r': [('o', 2)]})
        ['r', 'o', 'o', 'g', 'g']

        >>> get_inner_bags_for('b', {'b': [('r', 1)], 'r': [('o', 1)], 'o': [('y', 1)]})
        ['r', 'o', 'y']

        >>> get_inner_bags_for('b', {'b': [('r', 2)], 'r': [('o', 3)], 'v': [('o', 1)]})
        ['r', 'r', 'o', 'o', 'o', 'o', 'o', 'o']
    """
    contained_bags = sum(([x[0]] * x[1] + get_inner_bags_for(x[0], rules_tree) * x[1]
                          for x in rules_tree.get(color, [])), [])

    return contained_bags


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        rules = [x.strip() for x in input_data.readlines()]

    rules_tree = parse_rules(rules)
    outer_bags = get_outer_bags_for('shiny gold', rules_tree)
    inner_bags = get_inner_bags_for('shiny gold', rules_tree)

    if getattr(args, 'verbose', False):
        print(f'There are {len(outer_bags)} that can contain shiny gold bags: {outer_bags}')
        print(f'There are {len(inner_bags)} in "shiny gold" bags: {inner_bags}')
    else:
        print(f'There are {len(outer_bags)} that can contain shiny gold bags')
        print(f'There are {len(inner_bags)} in "shiny gold" bags')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 7: Handy Haversacks')
    args_parser.add_argument('input_file', type=str)
    args_parser.add_argument('-v', '--verbose', action='store_true')
    args = args_parser.parse_args()
    main(args)
