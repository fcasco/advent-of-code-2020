import argparse


def get_next_bus(timestamp, buses):
    """
        >>> get_next_bus(939, ['7', '13', 'x', 'x', '59', 'x', '31', '19'])
        ('59', 5)
    """
    minutes_to_wait = None
    best_bus = None
    for bus in buses:
        if bus == 'x':
            continue

        bus_period = int(bus)
        nearest_arrival = (timestamp // bus_period + 1) * bus_period
        new_minutes_to_wait = nearest_arrival - timestamp
        if minutes_to_wait is None or new_minutes_to_wait < minutes_to_wait:
            minutes_to_wait = new_minutes_to_wait
            best_bus = bus

    return best_bus, minutes_to_wait


def get_part_2_timestamp(buses):
    """
        >>> get_part_2_timestamp(['17', 'x', '13', '19'])
        3417

        >>> get_part_2_timestamp(['67', '7', 'x', '59', '61'])
        1261476

        >>> get_part_2_timestamp(['7', '13', 'x', 'x', '59', 'x', '31', '19'])
        1068781
    """
    ids_offsets = sorted([(int(x), i) for i, x in enumerate(buses) if x != 'x'])

    step = ids_offsets[0][0]
    timestamp = step - ids_offsets[0][1]
    for bus_id, offset in ids_offsets[1:]:
        while (timestamp + offset) % bus_id > 0:
            timestamp += step

        step *= bus_id

    return timestamp


def main(args):
    with open(args.input_file) as input_data:
        timestamp_data, buses_data = input_data.readlines()
    timestamp = int(timestamp_data.strip())
    buses = buses_data.strip().split(',')

    bus_id, minutes = get_next_bus(timestamp, buses)

    print(f'Best bus is {bus_id} at {minutes}, solution {int(bus_id) * minutes}')

    part_2_timestamp = get_part_2_timestamp(buses)
    print(f'Part 2 timestamp is {part_2_timestamp}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 13: Shuttle Search')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
