import argparse
import itertools


def find_bad_number(numbers, preamble_len=25):
    """
        >>> find_bad_number((
        ... 35,
        ... 20,
        ... 15,
        ... 25,
        ... 47,
        ... 40,
        ... 62,
        ... 55,
        ... 65,
        ... 95,
        ... 102,
        ... 117,
        ... 150,
        ... 182,
        ... 127,
        ... 219,
        ... 299,
        ... 277,
        ... 309,
        ... 576,
        ... ), 5)
        127
    """
    all_valid = True
    for i, n in enumerate(numbers[preamble_len:]):
        valid_operands = set(numbers[i:i + preamble_len])
        valid_numbers = set([x + y for x, y in itertools.combinations(valid_operands, 2)])
        if n not in valid_numbers:
            all_valid = False
            break

    return n if not all_valid else None


def find_weakness(numbers, preamble_len=25):
    """
        >>> find_weakness((
        ... 35,
        ... 20,
        ... ), 5)

        >>> find_weakness((
        ... 1,
        ... 2,
        ... 3,
        ... 5,
        ... 6,
        ... ), 2)
        4

        >>> find_weakness((
        ... 35,
        ... 20,
        ... 15,
        ... 25,
        ... 47,
        ... 40,
        ... 62,
        ... 55,
        ... 65,
        ... 95,
        ... 102,
        ... 117,
        ... 150,
        ... 182,
        ... 127,
        ... 219,
        ... 299,
        ... 277,
        ... 309,
        ... 576,
        ... ), 5)
        62
    """
    bad_number = find_bad_number(numbers, preamble_len)
    if bad_number is None:
        return None

    numbers_len = len(numbers)
    for i in range(numbers_len - 1):
        i_number = numbers[i]
        acc = i_number
        smallest = i_number
        largest = i_number
        for f in range(i + 1, numbers_len):
            f_number = numbers[f]

            if f_number < smallest:
                smallest = f_number
            if f_number > largest:
                largest = f_number

            acc += f_number
            if acc >= bad_number:
                break

        if acc == bad_number:
            break

    return smallest + largest if acc == bad_number else None


def main(args):
    with open(args.input_file) as input_data:
        numbers = [int(x) for x in input_data]

    bad_number = find_bad_number(numbers, 25)
    if bad_number is not None:
        print(f'Bad number found: {bad_number}')

    weakness = find_weakness(numbers, 25)
    if weakness is not None:
        print(f'Weakness found: {weakness}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 9: Encoding Error')
    args_parser.add_argument('input_file', type=str)

    args = args_parser.parse_args()
    main(args)
