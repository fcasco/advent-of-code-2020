"""
Day 17: Conway Cubes
"""
import argparse
import collections
import itertools


class Machine3d:
    directions = [x for x in itertools.product(*[(-1, 0, 1)] * 3) if any(v != 0 for v in x)]
    unknown_char = '?'
    active_char = '#'
    inactive_char = '.'
    active_at = (2, 3)

    def __init__(self, initial_state):
        self.initial_state = initial_state
        self.size = {'x': [0, 0], 'y': [0, 0], 'z': [0, 0]}
        self.set_state(initial_state)

    def get_section(self, dx, dy, dz):
        """
            >>> Machine3d([]).get_section(dx=(0, 3), dy=(0, 3), dz=(0, 1))
            '...\\n...\\n...'
        """
        return '\n\n'.join('\n'.join(''.join(self.state[x][y][z]
                                             for x in range(dx[0], dx[1]))
                                     for y in range(dy[0], dy[1]))
                           for z in range(dz[0], dz[1]))

    def make_empty_state(self):
        return collections.defaultdict(
                lambda: collections.defaultdict(
                    lambda: collections.defaultdict(lambda: self.inactive_char)))

    def set_state(self, state, z=0):
        """
            >>> m = Machine3d([])
            >>> m.set_state(['.#.', '..#', '###'])
            >>> print(m.get_section(dx=(0, 3), dy=(0, 3), dz=(0, 1)))
            .#.
            ..#
            ###
            >>> m.size
            {'x': [0, 3], 'y': [0, 3], 'z': [0, 1]}
        """
        new_state = self.make_empty_state()
        for y, line in enumerate(state):
            for x, cell in enumerate(line):
                new_state[x][y][z] = cell
        self.state = new_state

        self.size['y'][1] = len(state)
        if self.size['y'][1] > 0:
            self.size['z'][1] = z + 1
            self.size['x'][1] = len(state[0])

    def get_related_cells(self, x, y, z):
        """
            >>> m = Machine3d(['.#.', '..#', '###'])
            >>> cells = m.get_related_cells(x=1, y=2, z=0)
            >>> len([x for x in cells if x == m.active_char])
            3
            >>> len([x for x in cells if x == m.inactive_char])
            23
        """
        related_cells = []
        for d in self.directions:
            related_cells.append(self.state[x + d[0]][y + d[1]][z + d[2]])

        return related_cells

    def evolve_state(self):
        """
            >>> m = Machine3d(['.#.', '..#', '###'])
            >>> m.evolve_state()
            >>> m.get_section((0, 3), (0, 4), (-1, 0))
            '...\\n#..\\n..#\\n.#.'
            >>> m.get_section((0, 3), (0, 4), (0, 1))
            '...\\n#.#\\n.##\\n.#.'
            >>> m.get_section((0, 3), (0, 4), (1, 2))
            '...\\n#..\\n..#\\n.#.'
            >>> m.size
            {'x': [0, 3], 'y': [0, 4], 'z': [-1, 2]}
        """
        new_state = self.make_empty_state()
        x_range = range(self.size['x'][0] - 1, self.size['x'][1] + 1)
        y_range = range(self.size['y'][0] - 1, self.size['y'][1] + 1)
        z_range = range(self.size['z'][0] - 1, self.size['z'][1] + 1)
        for x in x_range:
            for y in y_range:
                for z in z_range:
                    old_cell = self.state[x][y][z]
                    new_cell = old_cell
                    related_cells = self.get_related_cells(x, y, z)
                    related_active_q = len([x for x in related_cells if x == self.active_char])

                    if new_cell == self.active_char and related_active_q not in self.active_at:
                        new_cell = self.inactive_char
                    elif new_cell == self.inactive_char and related_active_q == 3:
                        new_cell = self.active_char

                        if x < self.size['x'][0]:
                            self.size['x'][0] = x
                        elif self.size['x'][1] <= x:
                            self.size['x'][1] = x + 1

                        if y < self.size['y'][0]:
                            self.size['y'][0] = y
                        elif self.size['y'][1] <= y:
                            self.size['y'][1] = y + 1

                        if z < self.size['z'][0]:
                            self.size['z'][0] = z
                        elif self.size['z'][1] <= z:
                            self.size['z'][1] = z + 1

                    new_state[x][y][z] = new_cell

        self.state = new_state

    def get_active_count(self):
        """
            >>> Machine3d(['.#.', '..#', '###']).get_active_count()
            5
        """
        actives = 0
        for x in range(*self.size['x']):
            for y in range(*self.size['y']):
                for z in range(*self.size['z']):
                    if self.state[x][y][z] == self.active_char:
                        actives += 1
        return actives


class Machine4d:
    dimensions = 'xyzw'
    directions = [x for x in itertools.product(*[(-1, 0, 1)] * len(dimensions))
                  if any(v != 0 for v in x)]
    active_char = '#'
    inactive_char = '.'
    active_at = (2, 3)

    def __init__(self, initial_state):
        """
            >>> len(Machine4d([]).directions)
            80

            >>> Machine4d([]).directions[0]
            (-1, -1, -1, -1)

            >>> Machine4d([]).size
            {'x': [0, 0], 'y': [0, 0], 'z': [0, 0], 'w': [0, 0]}
        """
        self.initial_state = initial_state
        self.size = {d: [0, 0] for d in self.dimensions}
        self.set_state(initial_state)

    def get_section(self, dx, dy, dz, dw):
        """
            >>> m = Machine4d([])
            >>> m.inactive_char = 'o'
            >>> print(m.get_section((0, 3), (0, 3), (0, 2), (0, 2)))
            ooo
            ooo
            ooo
            <BLANKLINE>
            ooo
            ooo
            ooo
            <BLANKLINE>
            <BLANKLINE>
            ooo
            ooo
            ooo
            <BLANKLINE>
            ooo
            ooo
            ooo
        """
        return '\n\n\n'.join(
                    '\n\n'.join(
                        '\n'.join(''.join(self.state[x][y][z][w] for x in range(*dx))
                                  for y in range(*dy))
                        for z in range(*dz))
                    for w in range(*dw))

    def make_empty_state(self):
        """
            >>> Machine4d([]).make_empty_state()[0][1][2][3]
            '.'
        """
        return collections.defaultdict(
                lambda: collections.defaultdict(
                        lambda: collections.defaultdict(
                            lambda: collections.defaultdict(lambda: self.inactive_char))))

    def set_state(self, state):
        """
            >>> m = Machine4d([])
            >>> m.set_state(['.#.', '..#', '###'])
            >>> print(m.get_section(dx=(0, 3), dy=(0, 3), dz=(0, 1), dw=(0, 1)))
            .#.
            ..#
            ###
            >>> m.size
            {'x': [0, 3], 'y': [0, 3], 'z': [0, 1], 'w': [0, 1]}
        """
        z = 0
        w = 0
        new_state = self.make_empty_state()
        for y, line in enumerate(state):
            for x, cell in enumerate(line):
                new_state[x][y][z][w] = cell
        self.state = new_state

        self.size['y'][1] = len(state)
        if self.size['y'][1] > 0:
            self.size['x'][1] = len(state[0])
            self.size['z'] = [0, 1]
            self.size['w'] = [0, 1]

    def get_related_cells(self, x, y, z, w):
        """
            >>> m = Machine4d(['.#.', '..#', '###'])
            >>> cells = m.get_related_cells(x=1, y=2, z=0, w=0)
            >>> len([x for x in cells if x == m.active_char])
            3
            >>> len([x for x in cells if x == m.inactive_char])
            77
        """
        related_cells = []
        for d in self.directions:
            xv = self.state[x + d[0]]
            yv = xv[y + d[1]]
            zv = yv[z + d[2]]
            related_cells.append(zv[w + d[3]])

        return related_cells

    def evolve_state(self):
        """
            >>> m = Machine4d(['.#.', '..#', '###'])
            >>> m.evolve_state()
            >>> m.get_section((0, 3), (0, 4), (-1, 0), (0, 1))
            '...\\n#..\\n..#\\n.#.'
            >>> m.get_section((0, 3), (0, 4), (0, 1), (0, 1))
            '...\\n#.#\\n.##\\n.#.'
            >>> m.get_section((0, 3), (0, 4), (1, 2), (0, 1))
            '...\\n#..\\n..#\\n.#.'
            >>> m.size
            {'x': [0, 3], 'y': [0, 4], 'z': [-1, 2], 'w': [-1, 2]}
        """
        new_state = self.make_empty_state()
        x_range = range(self.size['x'][0] - 1, self.size['x'][1] + 1)
        y_range = range(self.size['y'][0] - 1, self.size['y'][1] + 1)
        z_range = range(self.size['z'][0] - 1, self.size['z'][1] + 1)
        w_range = range(self.size['w'][0] - 1, self.size['w'][1] + 1)
        for x in x_range:
            for y in y_range:
                for z in z_range:
                    for w in w_range:
                        old_cell = self.state[x][y][z][w]
                        new_cell = old_cell
                        related_cells = self.get_related_cells(x, y, z, w)
                        related_active_q = len([x for x in related_cells
                                                if x == self.active_char])

                        if new_cell == self.active_char \
                                and related_active_q not in self.active_at:
                            new_cell = self.inactive_char
                        elif new_cell == self.inactive_char \
                                and related_active_q == 3:
                            new_cell = self.active_char

                            if x < self.size['x'][0]:
                                self.size['x'][0] = x
                            elif self.size['x'][1] <= x:
                                self.size['x'][1] = x + 1

                            if y < self.size['y'][0]:
                                self.size['y'][0] = y
                            elif self.size['y'][1] <= y:
                                self.size['y'][1] = y + 1

                            if z < self.size['z'][0]:
                                self.size['z'][0] = z
                            elif self.size['z'][1] <= z:
                                self.size['z'][1] = z + 1

                            if w < self.size['w'][0]:
                                self.size['w'][0] = w
                            elif self.size['w'][1] <= w:
                                self.size['w'][1] = w + 1

                        new_state[x][y][z][w] = new_cell

        self.state = new_state

    def get_active_count(self):
        """
            >>> Machine4d(['.#.', '..#', '###']).get_active_count()
            5
        """
        actives = 0
        for x in range(*self.size['x']):
            for y in range(*self.size['y']):
                for z in range(*self.size['z']):
                    for w in range(*self.size['z']):
                        if self.state[x][y][z][w] == self.active_char:
                            actives += 1
        return actives


def main(args):
    with open(args.input_file) as input_data:
        data = [x.strip() for x in input_data.readlines()]

    machine3d = Machine3d(initial_state=data)
    for i in range(6):
        machine3d.evolve_state()
    print(f'The active count after {i} cycles is {machine3d.get_active_count()}')

    machine4d = Machine4d(initial_state=data)
    for i in range(6):
        machine4d.evolve_state()
    print(f'The active count after {i} cycles is {machine4d.get_active_count()}')


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser(description='Day 17: Conway Cubes')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
