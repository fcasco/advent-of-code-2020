


class MachineNd:
    dimensions = 'xyzw'
    active_char = '#'
    inactive_char = '.'
    active_at = (2, 3)

    def __init__(self, initial_state, dimensions='xyzw'):
        """
            >>> len(MachineNd([]).directions)
            80

            >>> MachineNd([]).size
            {'x': [0, 0], 'y': [0, 0], 'z': [0, 0], 'w': [0, 0]}
        """
        self.dimensions = dimensions
        self.initial_state = initial_state
        self.size = {d: [0, 0] for d in self.dimensions}
        self.set_state(initial_state)
        self.directions = [x for x in itertools.product(*[(-1, 0, 1)] * len(dimensions))
                           if any(v != 0 for v in x)]

    def get_section(self, ranges):
        """
            >>> MachineNd([]).get_section(((0, 3), (0, 3), (0, 3), (0, 1)))
            '...\\n...\\n...\\n\\n...\\n...\\n...\\n\\n...\\n...\\n...'
        """
        """
            >>> MachineNd(['.#.', '#.#', '...']).get_section(((0, 3), (0, 3), (0, 3), (0, 1)))
            '...\\n...\\n...\\n\\n...\\n...\\n...\\n\\n...\\n...\\n...'
        """
        # TODO: generalize to n dimensions
        if len(ranges) != 4:
            raise NotImplementedError

        separator = '\n'
        w_values = []
        for w in range(*ranges[3]):

            z_values = []
            for z in range(*ranges[2]):

                y_values = []
                for y in range(*ranges[1]):

                    x_values = []
                    for x in range(*ranges[0]):
                        x_values.append(self.state[x][y][z][w])
                    y_values.append((separator * 0).join(x_values))

                z_values.append((separator * 1).join(y_values))

            w_values.append((separator * 2).join(z_values))

        return (separator * 3).join(w_values)

    def make_empty_state(self):
        """
            >>> type(MachineNd([]).make_empty_state()[0][1][2])
            <class 'collections.defaultdict'>

            >>> MachineNd([]).make_empty_state()[0][1][2][3]
            '.'

            >>> s = MachineNd([]).make_empty_state()
            >>> s[0][0][0][0] == MachineNd.inactive_char
            True
            >>> s[0][0][0][0] = '#'
            >>> s[0][0][0][0]
            '#'
            >>> s[1][0][0][0]
            '.'
        """
        return collections.defaultdict(
                lambda: collections.defaultdict(
                    lambda: collections.defaultdict(
                        lambda: collections.defaultdict(lambda: self.inactive_char)
                    )
                )
        )

        empty_state = collections.defaultdict(lambda: self.inactive_char)
        for d in self.dimensions[:-1]:
            empty_state = (lambda x: collections.defaultdict(lambda: x))(empty_state)

        return empty_state

    def set_state(self, state):
        """
            >>> m = MachineNd([])
            >>> m.set_state(['.#.', '..#', '###'])
            >>> m.state[1][0][0][0]
            '#'
            >>> m.state[0][0][0][0]
            '.'
        """
        new_state = self.make_empty_state()
        v = []
        for y, yv in enumerate(state):
            for x, v in enumerate(yv):
                new_state[x][y][0][0] = v
        self.state = new_state

        self.size['y'][1] = len(state)
        if self.size['y'][1] > 0:
            self.size['x'][1] = len(state[0])
            self.size['z'][1] = 1
            self.size['w'][1] = 1

    def get_related_cells(self, x, y, z):
        """
            >>> m = MachineNd(['.#.', '..#', '###'])
            >>> cells = m.get_related_cells(1, 2, 0)
            >>> len([x for x in cells if x == m.active_char])
            3
            >>> len([x for x in cells if x == m.inactive_char])
            23
        """
        related_cells = []
        for d in self.directions:
            related_cells.append(self.state[x + d[0]][y + d[1]][z + d[2]])

        return related_cells
