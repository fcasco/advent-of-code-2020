import argparse


class Seats:
    """
        >>> Seats(['#', '#', '#']).height
        3

        >>> Seats(['#', '#', '#']).width
        1
    """
    def __init__(self, state, mode=1, top_limit=4):
        self.state = state
        self.top_limit = top_limit
        self.mode = mode

        self.height = len(self.state)
        self.width = len(self.state[0]) if self.height > 0 else 0

    def __iter__(self):
        return self

    def __next__(self):
        self.update()
        return self.state

    def count_occupied(self):
        """
            >>> Seats(['LLL.L', 'LLL.L', 'LLL.L']).count_occupied()
            0

            >>> Seats(['L#L.L', 'LLL.#', '#L#.L']).count_occupied()
            4
        """
        return sum(x.count('#') for x in self.state)

    def show_state(self):
        print('\n'.join(self.state))

    def get_adjacent_coords(self, x, y):
        """
            >>> Seats(['...', '...', '...']).get_adjacent_coords(0, 0)
            [(0, 1), (1, 0), (1, 1)]

            >>> Seats(['...', '...', '...']).get_adjacent_coords(1, 1)
            [(0, 0), (0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1), (2, 2)]

            >>> Seats(['...', '...', '...']).get_adjacent_coords(2, 2)
            [(1, 1), (1, 2), (2, 1)]

            >>> Seats(['.', '.', '.']).get_adjacent_coords(0, 1)
            [(0, 0), (0, 2)]

            >>> Seats(['....']).get_adjacent_coords(2, 0)
            [(1, 0), (3, 0)]
        """
        return [(i, j)
                for i in range(x + (-1 if x > 0 else 0), x + (2 if x + 1 < self.width else 1))
                for j in range(y + (-1 if y > 0 else 0), y + (2 if y + 1 < self.height else 1))
                if (i, j) != (x, y)]

    def get_seats_in_line(self, x, y):
        """
            >>> Seats(['...', '...', '...']).get_seats_in_line(0, 0)
            ''

            >>> Seats(['L#.', '#..', '..#']).get_seats_in_line(0, 0)
            '###'

            >>> Seats(['L#..L', '#..LL', '..#L#']).get_seats_in_line(1, 0)
            'LL#L'
        """
        directions = ((0, -1), (1, -1), (1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1))
        seats = []
        for direction in directions:
            d = 0
            position = None
            while position not in ('L', '#'):
                d += 1
                i = x + direction[0] * d
                j = y + direction[1] * d
                if i < 0 or j < 0:
                    position = None
                    break

                try:
                    position = self.state[j][i]
                except IndexError:
                    position = None
                    break

            if position in ('L', '#'):
                seats.append(position)

        return ''.join(seats)

    def update(self, raise_exception=True):
        """
            >>> Seats(
            ... ['LLL.L', 'LLL.L', 'LLL.L']
            ... ).update().show_state()
            ###.#
            ###.#
            ###.#

            >>> Seats(
            ... ['..', 'L#', '..']
            ... ).update(raise_exception=False).show_state()
            ..
            L#
            ..

            #.##.#
            ######
            >>> Seats(['#.##.#', '######'], 2, 5).update(raise_exception=False).show_state()
            #.LL.#
            ######
        """
        seat_changed = False
        new_state = []
        for y in range(self.height):
            new_row = []
            for x in range(self.width):
                current_seat = self.state[y][x]
                new_seat = current_seat

                if self.mode == 1:
                    adjacents = ''.join(self.state[j][i]
                                        for i, j in self.get_adjacent_coords(x, y))

                    if current_seat == 'L':
                        if all(x in ('L', '.') for x in adjacents):
                            new_seat = '#'

                    elif current_seat == '#':
                        if adjacents.count('#') >= self.top_limit:
                            new_seat = 'L'

                elif self.mode == 2:
                    occupied = self.get_seats_in_line(x, y).count('#')
                    if current_seat == 'L':
                        if not occupied:
                            new_seat = '#'

                    if current_seat == '#':
                        if occupied >= self.top_limit:
                            new_seat = 'L'

                if new_seat != current_seat:
                    seat_changed = True

                new_row.append(new_seat)

            new_state.append(''.join(new_row))

        if seat_changed:
            self.state = new_state
        else:
            if raise_exception:
                raise StopIteration

        return self


def main(args):
    with open(args.input_file) as input_data:
        seats_state = [x.strip() for x in input_data.readlines()]

    if getattr(args, 'interactive', False):
        seats = Seats(seats_state, mode=args.mode, top_limit=args.top)
        k = ''
        i = 0
        while k != 'n':
            seats.show_state()
            print(f'Iterations: {i}, Occupied: {seats.count_occupied()}')
            k = input('Next: [Y/n]? ')
            i += 1
            try:
                next(seats)
            except StopIteration:
                break

    else:
        seats_part1 = Seats(seats_state, mode=1, top_limit=4)
        iterations_1 = len(list(seats_part1))
        print(f'Part 1: occupied seats after {iterations_1}: {seats_part1.count_occupied()}')

        seats_part2 = Seats(seats_state, mode=2, top_limit=5)
        iterations_2 = len(list(seats_part2))
        print(f'Part 2: occupied seats after {iterations_2}: {seats_part2.count_occupied()}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 11: Seating System')
    args_parser.add_argument('input_file', type=str)
    args_parser.add_argument('-i', '--interactive', action='store_true')
    args_parser.add_argument('-m', '--mode', type=int, default=2)
    args_parser.add_argument('-t', '--top', type=int, default=5)
    args = args_parser.parse_args()
    main(args)
