import argparse
import re


class VM:
    address_re = re.compile(r'^mem\[(\d+)\]')
    value_re = re.compile(r'^.* = (.*)$')
    default_mask = 'X' * 36

    def __init__(self, data, mask=None):
        self.data = data
        self.memory = {}
        self.mask = self.default_mask if mask is None else mask

    @property
    def memory_sum(self):
        return sum(self.memory.values())

    def execute(self, instruction):
        """
            >>> VM(data=[]).execute('mem[8] = 11').memory[8]
            11
        """
        value = self.value_re.match(instruction)[1]
        address_match = self.address_re.match(instruction)
        if address_match:
            address = int(address_match[1])
            final_value = ''.join(m if m != 'X' else v
                                  for m, v in zip(self.mask, f'{int(value):036b}'))
            self.memory[address] = int(final_value, 2)
        else:
            self.mask = value

        return self

    def run(self):
        for instruction in self.data:
            self.execute(instruction)


class VM2(VM):
    default_mask = '0' * 36

    def get_addresses(self, instruction_address):
        """
            >>> VM2([], mask='000000000000000000000000000000000000').get_addresses(8)
            [8]

            >>> VM2([], mask='000000000000000000000000000000000001').get_addresses(8)
            [9]

            >>> VM2([], mask='00000000000000000000000000000000000X').get_addresses(8)
            [8, 9]

            >>> VM2([], mask='X0000000000000000000000000000000000X').get_addresses(0)
            [0, 34359738368, 1, 34359738369]
        """
        mask = self.mask.zfill(36)
        bin_address = [x if m != '1' else '1'
                       for x, m in zip(f'{instruction_address:036b}', mask)]
        floating_bits = [i for i, v in enumerate(self.mask) if v == 'X']

        addresses = []
        if floating_bits:
            mask_len = len(self.mask)
            for i in range(2 ** len(floating_bits)):
                bits = list(f'{i:0b}'.zfill(mask_len))
                addresses.append(int(''.join(bin_address[x] if x not in floating_bits
                                             else bits.pop() for x in range(mask_len)), 2))
            pass
        else:
            addresses.append(int(''.join(bin_address), 2))

        return addresses

    def execute(self, instruction):
        """
            >>> vm = VM2([])
            >>> vm.execute('mask = 000000000000000000000000000000X1001X')
            >>> vm.execute('mem[42] = 100')
            >>> [vm.memory[x] for x in (26, 27, 58, 59)]
            [100, 100, 100, 100]
        """
        value = self.value_re.match(instruction)[1]
        address_match = self.address_re.match(instruction)
        if address_match:
            addresses = self.get_addresses(int(address_match[1]))
            for address in addresses:
                self.memory[address] = int(value)
        else:
            self.mask = value


def main(args):
    with open(args.input_file) as input_data:
        data = [x.strip() for x in input_data.readlines()]

    vm = VM(data=data)
    vm.run()
    print(f'VM memory sum {vm.memory_sum}')

    vm2 = VM2(data=data)
    vm2.run()
    print(f'VM2 memory sum {vm2.memory_sum}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 0: Template')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
