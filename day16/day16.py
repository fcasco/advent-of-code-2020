import argparse
import re


class TicketProcessor:
    your_ticket_pre_line = 'your ticket:'
    nearby_ticket_pre_line = 'nearby tickets:'
    rule_range_re = re.compile(r'\b\d+-\d+\b')

    def __init__(self, raw_data):
        """
            >>> tp = TicketProcessor(raw_data=(
            ... 'class: 1-3 or 5-7',
            ... 'row: 6-11 or 33-44',
            ... 'seat: 13-40 or 45-50',
            ... '',
            ... 'your ticket:',
            ... '7,1,14',
            ... '',
            ... 'nearby tickets:',
            ... '7,3,47',
            ... '40,4,50',
            ... '55,2,20',
            ... '38,6,12',
            ... ))
            >>> tp.rules
            ['class: 1-3 or 5-7', 'row: 6-11 or 33-44', 'seat: 13-40 or 45-50']
            >>> tp.your_ticket
            '7,1,14'
            >>> tp.nearby_tickets
            ['7,3,47', '40,4,50', '55,2,20', '38,6,12']
        """
        self.parse_raw_data(raw_data)

    def parse_raw_data(self, raw_data):
        section = 'rules'
        self.rules = []
        self.your_ticket = ''
        self.nearby_tickets = []
        for line_data in raw_data:
            if not line_data.strip():
                continue

            if line_data == self.your_ticket_pre_line:
                section = 'your_ticket'
            elif line_data == self.nearby_ticket_pre_line:
                section = 'nearby_tickets'
            else:
                if section == 'rules':
                    self.rules.append(line_data)
                elif section == 'your_ticket':
                    self.your_ticket = line_data
                elif section == 'nearby_tickets':
                    self.nearby_tickets.append(line_data)

    def parse_rules(self):
        """
            >>> tp = TicketProcessor(raw_data=(
            ... 'class: 1-3 or 5-7',
            ... 'seat: 13-14',
            ... '',
            ... 'your ticket:',
            ... '7,1,14',
            ... '',
            ... 'nearby tickets:',
            ... '7,3,47',
            ... ))
            >>> tp.parse_rules()
            >>> tp.valid_numbers
            {1, 2, 3, 5, 6, 7, 13, 14}
            >>> tp.parsed_rules['class']
            {'valid_numbers': {1, 2, 3, 5, 6, 7}, 'position': None}
            >>> tp.parsed_rules['seat']
            {'valid_numbers': {13, 14}, 'position': None}
        """
        self.valid_numbers = set()
        self.parsed_rules = {}
        for rule in self.rules:
            rule_name, rule_ranges = rule.split(':')
            new_rule = {'valid_numbers': set(), 'position': None}
            rule_ranges = self.rule_range_re.findall(rule_ranges)
            for rule_range in rule_ranges:
                range_start, range_end = rule_range.split('-')
                range_set = set(range(int(range_start), int(range_end) + 1))
                self.valid_numbers |= range_set
                new_rule['valid_numbers'] |= range_set

            self.parsed_rules[rule_name] = new_rule

    def get_invalid_numbers(self):
        """
            >>> tp = TicketProcessor(raw_data=(
            ... 'class: 1-3 or 5-7',
            ... 'seat: 13-14',
            ... '',
            ... 'your ticket:',
            ... '7,1,14',
            ... '',
            ... 'nearby tickets:',
            ... '7,3,47',
            ... '7,3,14',
            ... ))
            >>> tp.get_invalid_numbers()
            [47]
        """
        self.parse_rules()
        invalid_numbers = []
        for ticket in self.nearby_tickets:
            invalid_numbers.extend(x for x in map(int, ticket.split(','))
                                   if x not in self.valid_numbers)
        return invalid_numbers

    def validate_ticket(self, ticket):
        ticket_numbers = map(int, ticket.split(','))
        is_valid = all(x in self.valid_numbers for x in ticket_numbers)
        return is_valid

    def get_valid_tickets(self):
        """
            >>> tp = TicketProcessor(raw_data=(
            ... 'class: 1-3 or 5-7',
            ... 'seat: 13-14',
            ... '',
            ... 'your ticket:',
            ... '7,14',
            ... '',
            ... 'nearby tickets:',
            ... '7,47',
            ... '7,14',
            ... ))
            >>> tp.get_valid_tickets()
            ['7,14']
        """
        self.parse_rules()
        valid_tickets = [x for x in self.nearby_tickets if self.validate_ticket(x)]
        return valid_tickets

    def get_ordered_fields(self):
        """
            >>> tp = TicketProcessor(raw_data=(
            ... 'class: 0-1 or 4-19',
            ... 'row: 0-5 or 8-19',
            ... 'seat: 0-13 or 16-19',
            ... '',
            ... 'your ticket:',
            ... '11,12,13',
            ... '',
            ... 'nearby tickets:',
            ... '3,9,18',
            ... '15,1,5',
            ... '5,14,9',
            ... ))
            >>> tp.get_ordered_fields()
            ['row', 'class', 'seat']
        """
        """

            >>> tp = TicketProcessor(raw_data=(
            ... 'class: 0-10',
            ... 'type: 0-4',
            ... 'row: 5-10',
            ... 'seat: 3-6',
            ... '',
            ... 'your ticket:',
            ... '1,2,3,4',
            ... '',
            ... 'nearby tickets:',
            ... '3,9,5,2',
            ... '50,1,1,1',
            ... ))
            >>> tp.get_ordered_fields()
            ['class', 'row', 'seat', 'type']
        """
        valid_tickets = self.get_valid_tickets()
        number_columns = []
        for ticket in valid_tickets:
            for i, number in enumerate(ticket.split(',')):
                try:
                    column = number_columns[i]
                except IndexError:
                    number_columns.append(set())
                    column = number_columns[i]
                column.add(int(number))

        for i, number in enumerate(list(map(int, self.your_ticket.split(',')))):
            number_columns[i].add(number)

        columns_matches = {i: [] for i in range(len(number_columns))}
        for i, column in enumerate(number_columns):
            columns_matches[i] = [name for name, rule in self.parsed_rules.items()
                                  if all(n in rule['valid_numbers'] for n in column)]

        for m in sorted(columns_matches.values(), key=lambda x: len(x)):
            if len(m) == 1:
                for n in columns_matches.values():
                    if m != n and m[0] in n:
                        n.remove(m[0])

        return [x[0] for x in columns_matches.values()]

    def get_ticket_product(self, ticket):
        """
            >>> tp = TicketProcessor(raw_data=(
            ... 'departure location: 0-1 or 4-19',
            ... 'row: 0-5 or 8-19',
            ... 'departure station: 0-13 or 16-19',
            ... '',
            ... 'your ticket:',
            ... '11,12,13',
            ... '',
            ... 'nearby tickets:',
            ... '3,9,18',
            ... '15,1,5',
            ... '5,14,9',
            ... ))
            >>> tp.get_ticket_product(tp.your_ticket)
            156

            >>> tp = TicketProcessor(raw_data=(
            ... 'departure location: 0-1 or 4-19',
            ... 'departure station: 0-13 or 16-19',
            ... 'row: 0-5 or 8-19',
            ... 'seat: 20-21',
            ... '',
            ... 'your ticket:',
            ... '11,12,13,20',
            ... '',
            ... 'nearby tickets:',
            ... '3,9,18,20',
            ... '15,1,5,21',
            ... '5,14,9,20',
            ... '50,14,9,21',
            ... ))
            >>> tp.get_ticket_product(tp.your_ticket)
            156

        """
        """
            >>> tp = TicketProcessor(raw_data=(
            ... 'departure location: 0-10',
            ... 'departure station: 0-4',
            ... 'row: 5-10',
            ... 'seat: 3-6',
            ... '',
            ... 'your ticket:',
            ... '1,2,3,4',
            ... '',
            ... 'nearby tickets:',
            ... '3,9,5,2',
            ... '50,1,1,1',
            ... ))
            >>> tp.get_ticket_product(tp.your_ticket)
            8
        """
        fields_prefix = 'departure'

        ticket_numbers = list(map(int, ticket.split(',')))
        ordered_fields = self.get_ordered_fields()
        product = 1
        for i, field in enumerate(ordered_fields):
            if field.startswith(fields_prefix):
                product *= ticket_numbers[i]

        return product

    def validate_tickets(self):
        valid_tickets = self.get_valid_tickets()
        self.get_ordered_fields()

        for name, rule in self.parsed_rules.items():
            print(f'{name} > {rule["position"]}')

        for ticket in valid_tickets:
            ticket_numbers = list(map(int, ticket.split(',')))
            print(f'Ticket to validate: {ticket}')
            for name, rule in self.parsed_rules.items():
                number = ticket_numbers[rule['position']]
                in_range = number in rule['valid_numbers']
                print(f'{name}: {number} is {"ok" if in_range else "bad"}')

            if input(':::') == 'q':
                break


def main(args):
    with open(args.input_file) as input_data:
        data = [x.strip() for x in input_data.readlines()]

    tickets_processor = TicketProcessor(raw_data=data)

    if getattr(args, 'debug', False):
        tickets_processor.validate_tickets()
    else:
        invalid_numbers = tickets_processor.get_invalid_numbers()
        print(f'The ticket scanning error rate is {sum(invalid_numbers)}')

        your_ticket = tickets_processor.your_ticket
        your_ticket_product = tickets_processor.get_ticket_product(your_ticket)
        print(f'The product of your ticket is {your_ticket_product}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 0: Template')
    args_parser.add_argument('input_file', type=str)
    args_parser.add_argument('-d', '--debug', action='store_true')
    args = args_parser.parse_args()

    main(args)
