import argparse
import importlib
import os
import sys


last_day = 19
solutions = {f'Day {x:02d}': importlib.import_module(f'day{x:02d}.day{x}')
             for x in range(1, last_day + 1)}


if __name__ == '__main__':
    print('Running Advent of Code 2020 solutions...')
    args_parser = argparse.ArgumentParser(description='Advent of Code 2020')
    args_parser.add_argument('-d', '--day', type=str, default='all',
                             help='number of day or all')
    args = args_parser.parse_args()

    exit_value = 0
    for day_name, day_module in solutions.items():
        if args.day != 'all' and day_name != f'Day {args.day}':
            continue

        day_path = os.path.split(day_module.__file__)[0]
        day_input_file = os.path.join(day_path, 'input.txt')
        day_args_parser = argparse.ArgumentParser()
        day_args_parser.add_argument('input_file', type=str)
        day_args = day_args_parser.parse_args((day_input_file, ))

        print(f'{day_name} with {day_args}')
        try:
            day_result = day_module.main(day_args)
        except Exception as e:
            print(f'{e}')
            exit_value = 1
        else:
            if day_result:
                print(day_result)
        print('-' * 24)

    sys.exit(exit_value)
