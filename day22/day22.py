"""
Day 22: Crab Combat
"""
import argparse
import functools


def parse_data(data):
    """
        >>> parse_data([
        ... 'Player 1:',
        ... '9',
        ... '2',
        ... '6',
        ... '3',
        ... '1',
        ... '',
        ... 'Player 2:',
        ... '5',
        ... '8',
        ... '4',
        ... '7',
        ... '10',
        ... ])
        [[9, 2, 6, 3, 1], [5, 8, 4, 7, 10]]
    """
    decks = []
    new_deck = []
    for line in data:
        if line.startswith('Player '):
            new_deck = []
        elif line:
            new_deck.append(int(line))
        else:
            decks.append(new_deck)
    decks.append(new_deck)
    return decks


def play_combat(decks, verbose=False):
    """
        >>> play_combat([[9, 2, 6, 3, 1], [5, 8, 4, 7, 10]])
        (1, [3, 2, 10, 6, 8, 5, 9, 4, 7, 1])
    """
    while all(len(x) > 0 for x in decks):
        if verbose:
            print(decks)

        playing_cards = [x.pop(0) for x in decks]
        top_card = -1
        winner = None
        for player, card in enumerate(playing_cards):
            if card > top_card:
                top_card = card
                winner = player

        if winner is None:
            print('tie')
            break

        decks[winner].extend(sorted(playing_cards, reverse=True))
    winner_player, winner_deck = [x for x in enumerate(decks) if len(x[1]) > 0][0]
    return winner_player, winner_deck


class RecursiveCombat:
    def __init__(self, decks=[], depth=0):
        self.decks = decks
        self.depth = depth
        self.winner = None
        self.previous_rounds = set()
        self.rounds = 0

    def play_round(self):
        """
            Repeated game status
            >>> combat = RecursiveCombat(decks=[[1, 2], [3, 4]])
            >>> combat.previous_rounds.add(str([[1, 2], [3, 4]]))
            >>> combat.play_round()
            >>> combat.winner
            0

            High card
            >>> combat = RecursiveCombat(decks=[[3, 2], [1, 4]])
            >>> combat.play_round()
            >>> combat.winner is None
            True
            >>> combat.decks
            [[2, 3, 1], [4]]

            Sub game
            >>> combat = RecursiveCombat(decks=[[1, 6, 3, 7], [2, 4, 5, 8]])
            >>> combat.play_round()
            >>> combat.winner is None
            True
            >>> combat.decks
            [[6, 3, 7, 1, 2], [4, 5, 8]]
        """
        if str(self.decks) in self.previous_rounds:
            self.winner = 0
            return

        self.previous_rounds.add(str(self.decks))

        cards_played = [x.pop(0) for x in self.decks]
        if any(len(self.decks[i]) < x for i, x in enumerate(cards_played)):
            winner, top_card = functools.reduce(lambda x, y: x if y[1] < x[1] else y,
                                                enumerate(cards_played))
            cards_played.sort(reverse=True)
        else:
            new_decks = [self.decks[i][:x] for i, x in enumerate(cards_played)]
            sub_game = RecursiveCombat(decks=new_decks, depth=self.depth + 1)
            winner, deck = sub_game.play_game()
            cards_played = [cards_played.pop(winner)] + cards_played

        self.decks[winner].extend(cards_played)

        remaining_players = [i for i, x in enumerate(self.decks) if len(x) > 0]
        if len(remaining_players) == 1:
            self.winner = remaining_players[0]

    def play_game(self):
        """
            Loop prevention
            >>> combat = RecursiveCombat(decks=[[43, 19], [2, 29, 14]])
            >>> combat.play_game()
            (0, [43, 19])
        """
        while self.winner is None:
            self.play_round()
            self.rounds += 1

        return self.winner, self.decks[self.winner]


def get_winner_score(deck):
    return sum(x * (i + 1) for i, x in enumerate(reversed(deck)))


def main(args):
    with open(args.input_file) as input_data:
        data = [x.strip() for x in input_data.readlines()]

    decks = parse_data(data)

    winner_player, winner_deck = play_combat([x[:] for x in decks])
    winner_score = get_winner_score(winner_deck)
    print(f'The player {winner_player} won the Combat game')
    print(f'with the deck {winner_deck}')
    print(f'and the score {winner_score}')

    recursive_combat_game = RecursiveCombat([x[:] for x in decks])
    winner_player, winner_deck = recursive_combat_game.play_game()
    winner_score = get_winner_score(winner_deck)
    print(f'The player {winner_player} won RecursiveCombat game')
    print(f'with the deck {winner_deck}')
    print(f'and the score {winner_score}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 22: Crab Combat')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
