import argparse


def check_password_1(rule, password):
    """
        >>> check_password_1('4-5 t', 'ftttttrvts')
        False

        >>> check_password_1('4-5 t', 'ftttttrvjs')
        True
    """
    counts, letter = rule.split(' ')
    min_count, max_count = counts.split('-')
    is_valid = int(min_count) <= password.count(letter) <= int(max_count)
    return is_valid


def check_password_2(rule, password):
    """
        >>> check_password_2('1-3 a', 'abcde')
        True

        >>> check_password_2('1-3 b', 'cdefg')
        False

        >>> check_password_2('2-9 c', 'ccccccccc')
        False

        >>> check_password_2('4-5 t', 'ftttttrvts')
        False

        >>> check_password_2('2-3 h', 'phhvhkhxhh')
        False
    """
    positions, letter = rule.split(' ')
    is_valid = len([x for x in positions.split('-') if password[int(x) - 1] == letter]) == 1
    return is_valid


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        valid_passwords_1 = []
        valid_passwords_2 = []
        for data in input_data.readlines():
            rule, password = data.split(':')
            password = password.strip()

            if check_password_1(rule, password):
                valid_passwords_1.append(password)

            if check_password_2(rule, password):
                valid_passwords_2.append(password)

    print(f'Count of valid passwords 1: {len(valid_passwords_1)}')
    print(f'Count of valid passwords 2: {len(valid_passwords_2)}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 2: Password Philosophy')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
