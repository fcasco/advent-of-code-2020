import argparse
import functools


class HexFloor:
    directions = {
        'se': (-1, 1),
        'sw': (1, 1),
        'nw': (1, -1),
        'ne': (-1, -1),
        'e': (-2, 0),
        'w': (2, 0),
    }

    def __init__(self):
        self.tiles = set()

    def flip_tile(self, path):
        """
            >>> floor = HexFloor()
            >>> floor.flip_tile('esenee')
            >>> floor.tiles
            {(-6, 0)}

            >>> floor = HexFloor()
            >>> floor.flip_tile('nwwswee')
            >>> floor.tiles
            {(0, 0)}

            >>> floor = HexFloor()
            >>> floor.flip_tile('sesenwnenenewseeswwswswwnenewsewsw')
            >>> floor.flip_tile('sesenwnenenewseeswwswswwnenewsesww')
            >>> floor.tiles
            set()
        """
        tile_position = (0, 0)
        while path:
            direction_found = False
            for direction, delta in self.directions.items():
                if path.startswith(direction):
                    tile_position = (tile_position[0] + delta[0], tile_position[1] + delta[1])
                    path = path[len(direction):]
                    direction_found = True
            if not direction_found:
                print('bad path')
                break

        if tile_position in self.tiles:
            self.tiles.remove(tile_position)
        else:
            self.tiles.add(tile_position)

    def flip_tiles(self, tiles_paths):
        """
            >>> floor = HexFloor()
            >>> floor.flip_tiles([
            ... 'sesenwnenenewseeswwswswwnenewsewsw',
            ... 'neeenesenwnwwswnenewnwwsewnenwseswesw',
            ... 'seswneswswsenwwnwse',
            ... 'nwnwneseeswswnenewneswwnewseswneseene',
            ... 'swweswneswnenwsewnwneneseenw',
            ... 'eesenwseswswnenwswnwnwsewwnwsene',
            ... 'sewnenenenesenwsewnenwwwse',
            ... 'wenwwweseeeweswwwnwwe',
            ... 'wsweesenenewnwwnwsenewsenwwsesesenwne',
            ... 'neeswseenwwswnwswswnw',
            ... 'nenwswwsewswnenenewsenwsenwnesesenew',
            ... 'enewnwewneswsewnwswenweswnenwsenwsw',
            ... 'sweneswneswneneenwnewenewwneswswnese',
            ... 'swwesenesewenwneswnwwneseswwne',
            ... 'enesenwswwswneneswsenwnewswseenwsese',
            ... 'wnwnesenesenenwwnenwsewesewsesesew',
            ... 'nenewswnwewswnenesenwnesewesw',
            ... 'eneswnwswnwsenenwnwnwwseeswneewsenese',
            ... 'neswnwewnwnwseenwseesewsenwsweewe',
            ... 'wseweeenwnesenwwwswnew',
            ... ])
            >>> len(floor.tiles)
            10
        """
        for path in tiles_paths:
            self.flip_tile(path)


class LivingFloor:
    directions = {
        'se': (-1, 1),
        'sw': (1, 1),
        'nw': (1, -1),
        'ne': (-1, -1),
        'e': (-2, 0),
        'w': (2, 0),
    }

    def __init__(self, black_tiles=None):
        if black_tiles is None:
            black_tiles = set()
        self.black_tiles = black_tiles

    @functools.cache
    def get_adjacent_tiles(self, tile):
        """
            >>> sorted(LivingFloor().get_adjacent_tiles((0, 0)))
            [(-2, 0), (-1, -1), (-1, 1), (1, -1), (1, 1), (2, 0)]
        """
        return set((tile[0] + x[0], tile[1] + x[1]) for x in self.directions.values())

    def paint_it_black(self, tile):
        """
            >>> floor = LivingFloor(black_tiles={(-2, 0), (0, 0), (2, 0)})
            >>> floor.paint_it_black((0, 0))
            True
            >>> floor.paint_it_black((-2, 0))
            True
            >>> floor.paint_it_black((-1, 1))
            True
            >>> floor.paint_it_black((1, 1))
            True
            >>> floor.paint_it_black((3, 0))
            False
            >>> floor.paint_it_black((-4, 0))
            False
        """
        adjacent_tiles = self.get_adjacent_tiles(tile)
        blacks_len = len(adjacent_tiles & self.black_tiles)

        return (1 <= blacks_len <= 2) if tile in self.black_tiles else (blacks_len == 2)

    def update(self, days=1, verbose=False):
        """
            >>> floor = LivingFloor(black_tiles={(0, 0)})
            >>> floor.update()
            >>> sorted(floor.black_tiles)
            []

            >>> floor = LivingFloor(black_tiles={(-2, 0), (0, 0), (2, 0)})
            >>> floor.update()
            >>> sorted(floor.black_tiles)
            [(-2, 0), (-1, -1), (-1, 1), (0, 0), (1, -1), (1, 1), (2, 0)]
        """
        for day in range(days):
            if verbose:
                print(f'Black tiles at day {day + 1}: {len(self.black_tiles)}')

            new_black_tiles = set()
            checked_tiles = set()
            for black_tile in self.black_tiles:
                adjacent_tiles = self.get_adjacent_tiles(black_tile)
                for adjacent_tile in adjacent_tiles:
                    if self.paint_it_black(adjacent_tile):
                        new_black_tiles.add(adjacent_tile)
                    checked_tiles.add(adjacent_tile)

                if black_tile in checked_tiles:
                    continue

                if self.paint_it_black(black_tile):
                    new_black_tiles.add(black_tile)
                checked_tiles.add(black_tile)

            self.black_tiles = new_black_tiles


def main(args):
    with open(args.input_file) as input_data:
        data = [x.strip() for x in input_data.readlines()]

    floor = HexFloor()
    floor.flip_tiles(data)
    print(f'The starting count of black tiles is {len(floor.tiles)}')

    living_floor = LivingFloor(black_tiles=floor.tiles)
    living_floor.update(days=100)
    print(f'The count of black tiles after 100 days is {len(living_floor.black_tiles)}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 0: Template')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
