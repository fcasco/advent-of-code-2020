import argparse


def solveit_for_2(numbers):
    """
        >>> solveit_for_2([2000, 2001, 20])
        (2000, 20)
    """
    answer = False
    for i, n in enumerate(numbers):
        if n > 2020:
            continue
        for m in numbers[i + 1:]:
            if n + m == 2020:
                answer = True
                break
        if answer:
            break

    return n, m


def solveit_for_3(numbers):
    """
        >>> solveit_for_3([2000, 2001, 20, 18, 1])
        (2001, 18, 1)

        >>> solveit_for_3([1, 20, 2000, 2001, 14, 20, 18, 1])
        (1, 2001, 18)
    """
    target = 2020
    solved = False
    i = -1
    numbers_count = len(numbers) - 1
    while not solved and i < numbers_count:
        i += 1
        n = numbers[i]
        j = i
        while not solved and j < numbers_count:
            j += 1
            m = numbers[j]
            k = j
            while not solved and k < numbers_count:
                k += 1
                o = numbers[k]
                if n + m + o == target:
                    solved = True

    if not solved:
        n = None
        m = None
        o = None

    return n, m, o


def main(args):
    report_file = args.input_file
    with open(report_file) as report_data:
        numbers = list(int(x) for x in report_data.readlines())
        n2, m2 = solveit_for_2(numbers)
        n3, m3, o3 = solveit_for_3(numbers)

    print(f'Answer for 2: {n2} * {m2} = {n2 * m2}')
    print(f'Answer for 3: {n3} * {m3} * {o3} = {n3 * m3 * o3}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(descrition='Day 1: Report Repair')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
