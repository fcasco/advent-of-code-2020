import argparse
import curses


def parse_boarding_pass(boarding_pass):
    """
        >>> parse_boarding_pass('FFFFFFFLLL')
        (0, 0, 0)

        >>> parse_boarding_pass('BFFFBBFRRR')
        (70, 7, 567)

        >>> parse_boarding_pass('BBBBBBBRRR')
        (127, 7, 1023)
    """
    top = 127
    bottom = 0
    pointer = 0
    while top != bottom:
        middle = (top - bottom) // 2 + bottom
        letter = boarding_pass[pointer]
        if letter == 'F':
            top = middle
        else:
            bottom = middle + 1
        pointer += 1
    row = top

    top = 7
    bottom = 0
    while top != bottom:
        middle = (top - bottom) // 2 + bottom
        letter = boarding_pass[pointer]
        if letter == 'L':
            top = middle
        else:
            bottom = middle + 1
        pointer += 1
    column = top

    seat_id = row * 8 + column

    return row, column, seat_id


def get_seat_id(boarding_pass):
    """
        >>> get_seat_id('FFFFFFFLLL')
        0

        >>> get_seat_id('BFFFBBFRRR')
        567

        >>> get_seat_id('BBBBBBBRRR')
        1023
    """
    top = 127
    bottom = 0
    pointer = 0
    while top != bottom:
        middle = (top - bottom) // 2 + bottom
        letter = boarding_pass[pointer]
        if letter == 'F':
            top = middle
        else:
            bottom = middle + 1
        pointer += 1
    row = top

    top = 7
    bottom = 0
    while top != bottom:
        middle = (top - bottom) // 2 + bottom
        letter = boarding_pass[pointer]
        if letter == 'L':
            top = middle
        else:
            bottom = middle + 1
        pointer += 1
    column = top

    seat_id = row * 8 + column

    return seat_id


def get_seat_ids(boarding_passes):
    return [parse_boarding_pass(x.strip())[2] for x in boarding_passes]


def get_highest_seat_id(boarding_passes):
    """
        >>> get_highest_seat_id((
        ... 'BFFFBBFRRR',
        ... ))
        567

        >>> get_highest_seat_id((
        ... 'BBFFBBFRLL',
        ... ))
        820

        >>> get_highest_seat_id((
        ... 'FFFBBBFRRR',
        ... 'BFFFBBFRRR',
        ... 'BBFFBBFRLL',
        ... ))
        820
    """
    return max(get_seat_ids(boarding_passes))


def get_missing_seats(boarding_passes):
    all_seat_ids = set(range(1023))
    taken_seats = set(parse_boarding_pass(x.strip())[2] for x in boarding_passes)
    return all_seat_ids - taken_seats


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        boarding_passes = input_data.readlines()

        highest_seat_id = get_highest_seat_id(boarding_passes)

        missing_seats = get_missing_seats(boarding_passes)

    print(f'Highest seat ID: {highest_seat_id}')
    print(f'Missing seats: {missing_seats}')


def visual(screen, args):
    screen.clear()

    input_file = args.input_file
    with open(input_file) as input_data:
        boarding_passes = input_data.readlines()

    pad = curses.newpad(20, 130)
    for boarding_pass in boarding_passes:
        boarding_pass = boarding_pass.strip()
        row, col, seat_id = parse_boarding_pass(boarding_pass)

        screen.addstr(0, 0, f'{boarding_pass} -> {seat_id}')
        pad.addch(col, row, '+')
        pad.refresh(0, 0, 1, 0, 20, 130)

        if screen.getkey() == 'q':
            break

    screen.refresh()


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 5: Binary Boarding')
    args_parser.add_argument('input_file', type=str)
    args_parser.add_argument('-i', '--visual', action='store_true')
    args = args_parser.parse_args()

    if args.visual:
        curses.wrapper(visual, args)
    else:
        main(args)
