import argparse
import math


def count_trees(trees_map, start=(0, 0), slope=(3, 1)):
    """
        >>> count_trees('...\\n...\\n...')
        0

        >>> count_trees(
        ...     '..##.......\\n'
        ...     '#...#...#..\\n'
        ...     '.#....#..#.\\n'
        ...     '..#.#...#.#\\n'
        ...     '.#...##..#.\\n'
        ...     '..#.##.....\\n'
        ...     '.#.#.#....#\\n'
        ...     '.#........#\\n'
        ...     '#.##...#...\\n'
        ...     '#...##....#\\n'
        ...     '.#..#...#.#\\n'
        ... )
        7

        >>> count_trees(
        ...     '..##.......\\n'
        ...     '#...#...#..\\n'
        ...     '.#....#..#.\\n'
        ...     '..#.#...#.#\\n'
        ...     '.#...##..#.\\n'
        ...     '..#.##.....\\n'
        ...     '.#.#.#....#\\n'
        ...     '.#........#\\n'
        ...     '#.##...#...\\n'
        ...     '#...##....#\\n'
        ...     '.#..#...#.#\\n'
        ... , slope=(1, 1))
        2

        >>> count_trees(
        ...     '..##.......\\n'
        ...     '#...#...#..\\n'
        ...     '.#....#..#.\\n'
        ...     '..#.#...#.#\\n'
        ...     '.#...##..#.\\n'
        ...     '..#.##.....\\n'
        ...     '.#.#.#....#\\n'
        ...     '.#........#\\n'
        ...     '#.##...#...\\n'
        ...     '#...##....#\\n'
        ...     '.#..#...#.#\\n'
        ... , slope=(5, 1))
        3

        >>> count_trees(
        ...     '..##.......\\n'
        ...     '#...#...#..\\n'
        ...     '.#....#..#.\\n'
        ...     '..#.#...#.#\\n'
        ...     '.#...##..#.\\n'
        ...     '..#.##.....\\n'
        ...     '.#.#.#....#\\n'
        ...     '.#........#\\n'
        ...     '#.##...#...\\n'
        ...     '#...##....#\\n'
        ...     '.#..#...#.#\\n'
        ... , slope=(1, 2))
        2
    """
    x = start[0]
    y = start[1]
    trees_map = trees_map.split()
    map_h = len(trees_map)
    map_w = len(trees_map[0])

    trees_found = 0
    while y < map_h:
        if trees_map[y][x % map_w] == '#':
            trees_found += 1
        x += slope[0]
        y += slope[1]

    return trees_found


def main(args):
    input_file = args.input_file
    slopes_to_check = ((1, 1), (3, 1), (5, 1), (7, 1), (1, 2))
    with open(input_file) as input_data:
        trees_map = input_data.read()

        trees_count = {}
        for slope in slopes_to_check:
            trees_count[slope] = count_trees(trees_map, slope=slope)

    print(f'Trees count: {trees_count}')
    print(f'Combined trees count: {math.prod(trees_count.values())}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(descrition='Day 3: Toboggan Trajectory')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
