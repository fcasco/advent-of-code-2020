import argparse


class LoopException(Exception):
    pass


class Interpreter:
    def __init__(self, instructions):
        self.instructions = instructions
        self.accumulator = 0

    def i_nop(self, arg):
        pass

    def i_acc(self, arg):
        self.accumulator += int(arg)

    def i_jmp(self, arg):
        return int(arg)

    def parse(self, instruction):
        method_name, raw_arg = instruction.split(' ')

        method = getattr(self, f'i_{method_name}')
        arg = int(raw_arg)

        return method, arg

    def reset(self):
        self.accumulator = 0

    def execute(self, instructions):
        instructions_len = len(self.instructions)
        instructions_pointer = 0
        instructions_executed = set()

        while instructions_pointer < instructions_len:
            if instructions_pointer in instructions_executed:
                raise LoopException(f'Loop exception at {instructions_pointer}')

            instructions_executed.add(instructions_pointer)
            instruction = instructions[instructions_pointer]
            method, arg = self.parse(instruction)
            instructions_pointer += method(arg) or 1

    def fix_and_execute(self, instructions):
        line_to_fix = -1
        candidate_lines = (i for i, x in enumerate(instructions)
                           if x.split(' ')[0] in ('nop', 'jmp'))
        for i in candidate_lines:
            line_to_fix = instructions[i]
            replace_args = ('jmp', 'nop') if 'jmp' in line_to_fix else ('nop', 'jmp')
            new_line = line_to_fix.replace(*replace_args)

            candidate_instructions = instructions[:i] + [new_line] + instructions[i + 1:]

            try:
                self.execute(candidate_instructions)
            except LoopException:
                self.reset()
            else:
                break

    def start(self, try_fix=False):
        """
            >>> Interpreter([
            ... 'nop +1',
            ... 'acc +1',
            ... 'jmp +2',
            ... 'acc +1',
            ... ]).start().accumulator
            1

            >>> Interpreter([
            ... 'nop +0',
            ... 'acc +1',
            ... 'jmp +4',
            ... 'acc +3',
            ... 'jmp -3',
            ... 'acc -99',
            ... 'acc +1',
            ... 'jmp -4',
            ... 'acc +6',
            ... ]).start().accumulator
            5

            >>> Interpreter([
            ... 'nop +0',
            ... 'acc +1',
            ... 'jmp +4',
            ... 'acc +3',
            ... 'jmp -3',
            ... 'acc -99',
            ... 'acc +1',
            ... 'jmp -4',
            ... 'acc +6',
            ... ]).start(try_fix=True).accumulator
            8
        """
        try:
            self.execute(self.instructions)
        except LoopException:
            if try_fix:
                self.fix_and_execute(self.instructions)

        return self


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        instructions = [x.strip() for x in input_data.readlines()]

    interpreter = Interpreter(instructions)

    if getattr(args, 'fix', False):
        interpreter.start(try_fix=True)
        print(f'Accumulator (fixed): {interpreter.accumulator}')
    else:
        interpreter.start(try_fix=False)
        print(f'Accumulator: {interpreter.accumulator}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 8: Handheld Halting')
    args_parser.add_argument('input_file', type=str)
    args_parser.add_argument('fix', action='store_true')
    args = args_parser.parse_args()
    main(args)
