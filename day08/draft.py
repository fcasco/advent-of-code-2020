import argparse


def run(code):
    """
        >>> run((
        ... 'nop +1',
        ... 'acc +3',
        ... 'jmp +2',
        ... 'acc +3',
        ... ))
        3

        >>> run((
        ... 'nop +1',
        ... 'acc +3',
        ... 'jmp +2',
        ... 'acc +3',
        ... 'jmp -2',
        ... ))
        'loop'
    """
    lines_ran = set()
    code_len = len(code)
    acc = 0
    i = 0
    while i < code_len:
        if i in lines_ran:
            return 'loop'

        lines_ran.add(i)
        op, arg = code[i].split(' ')

        if op == 'acc':
            acc += int(arg)
            i += 1
        elif op == 'jmp':
            i += int(arg)
        else:
            i += 1

    return acc


def part2(code):
    """
        >>> part2([
        ... 'nop +1',
        ... 'acc +3',
        ... 'jmp +2',
        ... 'acc +3',
        ... ])
        3

        >>> part2([
        ... 'nop +1',
        ... 'acc +3',
        ... 'jmp +2',
        ... 'acc +3',
        ... 'jmp -2',
        ... ])
        3

        >>> part2([
        ... 'nop +0',
        ... 'acc +1',
        ... 'jmp +4',
        ... 'acc +3',
        ... 'jmp -3',
        ... 'acc -99',
        ... 'acc +1',
        ... 'jmp -4',
        ... 'acc +6',
        ... ])
        8
    """
    line_to_fix = -1
    lines_to_fix = (i for i, x in enumerate(code) if x.split(' ')[0] in ('nop', 'jmp'))
    acc = run(code)
    if acc != 'loop':
        return acc

    for i in lines_to_fix:
        line_to_fix = code[i]
        if line_to_fix.startswith('n'):
            new_line = line_to_fix.replace('nop', 'jmp')
        else:
            new_line = line_to_fix.replace('jmp', 'nop')
        new_code = code[:i] + [new_line, ] + code[i + 1:]
        acc = run(new_code)
        if acc != 'loop':
            return acc

    return run(code)


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        code = [x.strip() for x in input_data.readlines()]

    acc = part2(code)

    print(f'Accumulator after fix: {acc}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 8: Handheld Halting')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
