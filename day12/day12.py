import argparse


class ShipOne:

    def __init__(self, moves, heading='E', position=(0, 0)):
        self.moves = moves
        self.position = position
        self.heading = heading

    def __repr__(self):
        return f'Ship: at {self.position} heading {self.heading}'

    def get_direction(self, heading=None):
        if heading is None or heading == 'F':
            heading = self.heading
        directions = {
                'N': (0, -1),
                'E': (1, 0),
                'S': (0, 1),
                'W': (-1, 0),
        }
        return directions[heading]

    def do_move(self, move):
        """
            >>> ShipOne(['F4']).run()
            Ship: at (4, 0) heading E

            >>> ShipOne(['N3']).run()
            Ship: at (0, -3) heading E

            >>> ShipOne(['S5']).run()
            Ship: at (0, 5) heading E

            >>> ShipOne(['E2']).run()
            Ship: at (2, 0) heading E

            >>> ShipOne(['W4']).run()
            Ship: at (-4, 0) heading E

            >>> ShipOne(['R90']).run()
            Ship: at (0, 0) heading S

            >>> ShipOne(['L270']).run()
            Ship: at (0, 0) heading S

            >>> ShipOne(['R90'], heading='N').run()
            Ship: at (0, 0) heading E

            >>> ShipOne(['R180']).run()
            Ship: at (0, 0) heading W
        """
        action = move[0]
        amount = int(move[1:])
        if action in 'FNSWE':
            direction = self.get_direction(action)
            self.position = (self.position[0] + direction[0] * amount,
                             self.position[1] + direction[1] * amount)
        elif action in 'RL':
            headings = 'ESWN'
            delta = amount // 90 * (-1 if action == 'L' else 1)
            self.heading = headings[(headings.find(self.heading) + delta) % 4]
        else:
            raise Exception(f'bad move {move}')

    def run(self, verbose=False):
        """
            >>> ShipOne(['F10', 'N3', 'F7', 'R90', 'F11']).run()
            Ship: at (17, 8) heading S

            >>> ShipOne(['R90', 'R90', 'R90', 'R90', 'L90', 'L90', 'L90', 'L90']).run()
            Ship: at (0, 0) heading E

            >>> ShipOne(['R180', 'F3']).run()
            Ship: at (-3, 0) heading W
        """
        for move in self.moves:
            if verbose:
                old_position = self.position
                old_heading = self.heading

            self.do_move(move)

            if verbose:
                print(f'{old_position} {old_heading} + {move} > {self.position} {self.heading}')

        return self

    @property
    def distance(self):
        return abs(self.position[0]) + abs(self.position[1])


class ShipTwo:

    def __init__(self, moves, heading='E', position=(0, 0), waypoint=(10, -1)):
        self.moves = moves
        self.position = position
        self.heading = heading
        self.waypoint = waypoint

    def __repr__(self):
        return f'Ship: {self.heading}{self.position} {self.waypoint}'

    def get_direction(self, heading=None):
        if heading is None or heading == 'F':
            heading = self.heading
        directions = {
                'N': (0, -1),
                'E': (1, 0),
                'S': (0, 1),
                'W': (-1, 0),
        }
        return directions[heading]

    def do_move(self, move):
        """
            >>> ShipTwo(['F4']).run()
            Ship: E(40, -4) (10, -1)

            >>> ShipTwo(['N3']).run()
            Ship: E(0, 0) (10, -4)

            >>> ShipTwo(['S5']).run()
            Ship: E(0, 0) (10, 4)

            >>> ShipTwo(['E2']).run()
            Ship: E(0, 0) (12, -1)

            >>> ShipTwo(['W4']).run()
            Ship: E(0, 0) (6, -1)

            >>> ShipTwo(['R90']).run()
            Ship: E(0, 0) (1, 10)

            >>> ShipTwo(['L270']).run()
            Ship: E(0, 0) (1, 10)

            >>> ShipTwo(['R180']).run()
            Ship: E(0, 0) (-10, 1)
        """
        action = move[0]
        amount = int(move[1:])
        if action in 'F':
            self.position = (self.position[0] + self.waypoint[0] * amount,
                             self.position[1] + self.waypoint[1] * amount)
        elif action in 'NSWE':
            direction = self.get_direction(action)
            self.waypoint = (self.waypoint[0] + direction[0] * amount,
                             self.waypoint[1] + direction[1] * amount)
        elif action in 'RL':
            if action == 'R':
                for i in range(amount // 90):
                    self.waypoint = (self.waypoint[1] * -1, self.waypoint[0])
            if action == 'L':
                for i in range(amount // 90):
                    self.waypoint = (self.waypoint[1], self.waypoint[0] * -1)
        else:
            raise Exception(f'bad move {move}')

    def run(self, verbose=False):
        """
            >>> ShipTwo(['F10', 'N3', 'F7', 'R90', 'F11']).run()
            Ship: E(214, 72) (4, 10)

            >>> ShipTwo(['R90', 'R90', 'R90', 'R90', 'L90', 'L90', 'L90', 'L90']).run()
            Ship: E(0, 0) (10, -1)

            >>> ShipTwo(['R180', 'F3']).run()
            Ship: E(-30, 3) (-10, 1)
        """
        for move in self.moves:
            if verbose:
                old_position = self.position
                old_heading = self.heading

            self.do_move(move)

            if verbose:
                print(f'{old_position} {old_heading} + {move} > {self.position} {self.heading}')

        return self

    @property
    def distance(self):
        return abs(self.position[0]) + abs(self.position[1])


def main(args):
    with open(args.input_file) as input_data:
        moves = [x.strip() for x in input_data.readlines()]

    verbose = getattr(args, 'verbose', False)
    ship_one = ShipOne(moves=moves)
    ship_one.run(verbose)

    print(f'Part 1: The final position is {ship_one.position}, heading {ship_one.heading} '
          f'with distance is {ship_one.distance}')

    ship_two = ShipTwo(moves=moves)
    ship_two.run(verbose)

    print(f'Part 2: The final position is {ship_two.position}, heading {ship_two.heading} '
          f'with distance is {ship_two.distance}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 12: Rain Risk')
    args_parser.add_argument('input_file', type=str)
    args_parser.add_argument('-v', '--verbose', action='store_true')
    args_parser.add_argument('-i', '--interactive', action='store_true')
    args = args_parser.parse_args()
    main(args)
