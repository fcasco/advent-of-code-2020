import doctest

import aoc2020solutions


def load_tests(loader, tests, ignore):
    for day in aoc2020solutions.solutions.values():
        tests.addTests(doctest.DocTestSuite(day))
    return tests
