import setuptools


with open('README.md', 'r', encoding='utf-8') as readme:
    long_description = readme.read()


setuptools.setup(
    name="advent-of-code-2020-fcasco",
    version="1.0.0",
    author="Facundo",
    author_email="fcasco@gmail.com",
    description="Solutions for advent of code 2020",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/fcasco/advent-of-code-2020/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
    python_requires='>=3.9',
)
