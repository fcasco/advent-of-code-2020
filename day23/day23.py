"""
Day 23: Crab Cups
"""
import argparse


def play_crab_game(cups, moves=100, verbose=False):
    """
        >>> play_crab_game([3, 8, 9, 1, 2, 5, 4, 6, 7], moves=1)
        [3, 2, 8, 9, 1, 5, 4, 6, 7]

        >>> play_crab_game([3, 8, 9, 1, 2, 5, 4, 6, 7], moves=2)
        [3, 2, 5, 4, 6, 7, 8, 9, 1]

        >>> play_crab_game([3, 8, 9, 1, 2, 5, 4, 6, 7], moves=3)
        [3, 4, 6, 7, 2, 5, 8, 9, 1]

        >>> play_crab_game([3, 8, 9, 1, 2, 5, 4, 6, 7], moves=4)
        [3, 2, 5, 8, 4, 6, 7, 9, 1]

        >>> play_crab_game([3, 8, 9, 1, 2, 5, 4, 6, 7], moves=6)
        [3, 6, 7, 2, 5, 8, 4, 1, 9]

        >>> play_crab_game([3, 8, 9, 1, 2, 5, 4, 6, 7], moves=9)
        [3, 9, 2, 6, 5, 7, 4, 1, 8]
    """
    cups_len = len(cups)
    cups_links = {cups[x]: cups[(x + 1) % cups_len] for x in range(cups_len)}
    cups_min = min(cups)
    cups_max = max(cups)

    current_cup = cups[-1]
    for move in range(moves):
        current_cup = cups_links[current_cup]

        selected_cups = [cups_links[current_cup]]
        for i in range(2):
            selected_cups.append(cups_links[selected_cups[-1]])

        cups_links[current_cup] = cups_links[selected_cups[-1]]

        target_cup = current_cup - 1
        while target_cup in selected_cups or target_cup not in cups:
            target_cup -= 1
            if target_cup < cups_min:
                target_cup = cups_max

        cups_links[selected_cups[-1]] = cups_links[target_cup]
        cups_links[target_cup] = selected_cups[0]

    cups = [cups[0]]
    for i in range(cups_len - 1):
        cups.append(cups_links[cups[-1]])

    return cups


def main(args):
    with open(args.input_file) as input_data:
        cups = [int(y) for y in [x.strip() for x in input_data.readlines()][0]]

    end_cups_1 = play_crab_game(cups, moves=100)
    cups_order_1 = end_cups_1[end_cups_1.index(1) + 1:] + end_cups_1[:end_cups_1.index(1)]
    print(f'The cups order for the part I is {"".join(str(x) for x in cups_order_1)}')

    part_two_count = 1000000
    part_two_moves = 10000000
    # part_two_moves = 100000
    part_two_cups = cups + list(range(max(cups) + 1, part_two_count + 1))
    end_cups_2 = play_crab_game(part_two_cups, moves=part_two_moves)
    index_1 = end_cups_2.index(1)
    print(f'{end_cups_2[index_1:index_1 + 4]}')
    # cups_order_2 = end_cups_2[end_cups_2.index(1) + 1:] + end_cups_2[:end_cups_1.index(2)]
    # print(f'The cups order for the part I is {"".join(str(x) for x in cups_order_1)}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 23: Crab Cups')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
