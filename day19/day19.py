"""
Day 19: Monster Messages
"""
import argparse
import re


class Validator:
    def __init__(self, data):
        """
            >>> v = Validator(['1', '2', '', 'a', 'b'])
            >>> v.raw_rules
            ['1', '2']
            >>> v.messages
            ['a', 'b']
        """
        self.raw_rules = []
        self.messages = []
        section = 'rules'
        for line in data:
            if line:
                if section == 'rules':
                    self.raw_rules.append(line)
                else:
                    self.messages.append(line)
            else:
                section = 'messages'

    def parse_rules(self):
        """
            >>> v = Validator([
            ... '0: 1 2',
            ... '1: "a"',
            ... '2: 1 3 | 3 1',
            ... '3: "b"',
            ... ])
            >>> v.parse_rules()
            >>> sorted(v.parsed_rules.items())
            [('0', '(a(ab|ba))'), ('1', 'a'), ('2', '(ab|ba)'), ('3', 'b')]

        """
        """
            # FIXME
            >>> v = Validator([
            ... '0: 1 0 | 0',
            ... '1: "a"',
            ... ])
            >>> v.parse_rules()
            >>> sorted(v.parsed_rules.items())
            [('0', '(a(ab|ba))'), ('1', 'a'), ('2', '(ab|ba)'), ('3', 'b')]
        """
        self.parsed_rules = {}
        rules_to_parse = [x.split(':') for x in self.raw_rules]
        i = 0
        while rules_to_parse:
            rule = rules_to_parse[i]
            if not any(x.isdigit() for x in rule[1]):
                rule_value = rule[1].strip('" ').replace(' ', '')

                if '|' in rule_value:
                    rule_value = f'({rule_value})'

                self.parsed_rules[rule[0]] = rule_value

                replace_re = re.compile(fr'\b{rule[0]}\b')
                rules_to_parse.pop(i)
                for rule_to_replace in rules_to_parse:
                    rule_to_replace[1] = replace_re.sub(rule_value, rule_to_replace[1])
                i = 0
            else:
                i += 1

    def get_valid_messages(self):
        """
            >>> v = Validator([
            ... '0: 4 1 5',
            ... '1: 2 3 | 3 2',
            ... '2: 4 4 | 5 5',
            ... '3: 4 5 | 5 4',
            ... '4: "a"',
            ... '5: "b"',
            ... '',
            ... 'ababbb',
            ... 'bababa',
            ... 'abbbab',
            ... 'aaabbb',
            ... 'aaaabbb',
            ... ])
            >>> v.get_valid_messages()
            ['ababbb', 'abbbab']
        """
        if not hasattr(self, 'parsed_rules'):
            self.parse_rules()

        rule_re = re.compile(f'^{self.parsed_rules["0"]}$')
        valid_messages = [x for x in self.messages if rule_re.match(x)]
        return valid_messages


class Validator2(Validator):
    def parse_rules(self):
        super().parse_rules()

        rule_31 = self.parsed_rules['31']
        rule_42 = self.parsed_rules['42']
        # rule 8 changed to 42 | 42 8
        self.parsed_rules['8'] = f'(({rule_42})+)'

        # rule 11 changed to 42 31 | 42 11 31
        # this is the ugliest hack I did
        # I should have learned how to make a recursive regex but didn't have the time
        # FIXME: use a recursive regex
        self.parsed_rules['11'] = f'({rule_42}{rule_31})' \
            + '|'.join(f'({rule_42 * x + rule_31 * x})' for x in range(10))

        self.parsed_rules['0'] = f'({self.parsed_rules["8"]})({self.parsed_rules["11"]})'


def main(args):
    with open(args.input_file) as input_data:
        data = [x.strip() for x in input_data.readlines()]

    validator = Validator(data)
    valid_messages = validator.get_valid_messages()
    print(f'The count of valid messages is {len(valid_messages)}')

    validator2 = Validator2(data)
    valid_messages2 = validator2.get_valid_messages()
    print(f'The count of valid messages for part 2 is {len(valid_messages2)}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 19: Monster Messages')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
