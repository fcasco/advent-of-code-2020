
import argparse


def get_sequence_number(sequence, position):
    """
        >>> get_sequence_number([0, 13, 1, 8, 6, 15], 6)
        15

        >>> get_sequence_number([0, 13, 1, 8, 6, 15], 7)
        0

        >>> get_sequence_number([0, 13, 1, 8, 6, 15], 8)
        6

        >>> get_sequence_number([0, 13, 1, 8, 6, 15], 9)
        3

        >>> [get_sequence_number([0, 3, 6], x) for x in range(4, 11)]
        [0, 3, 3, 1, 0, 4, 0]

        >>> get_sequence_number([0, 3, 6], 2020)
        436
    """
    sequence_len = len(sequence)
    if position < sequence_len - 1:
        return sequence[position - 1]

    spoken_numbers = set(sequence[:-1])
    for i in range(position - sequence_len):
        last_number = sequence[-1]
        if last_number not in spoken_numbers:
            sequence.append(0)
        else:
            for x, n in enumerate(reversed(sequence[:-1])):
                if n == last_number:
                    break
            sequence.append(x + 1)

        spoken_numbers.add(sequence[-2])

    return sequence[-1]


def get_sequence_number_2(sequence, position):
    """
        >>> get_sequence_number_2([0, 3, 6], 3)
        6

        >>> [get_sequence_number_2([0, 3, 6], x) for x in range(1, 7)]
        [0, 3, 6, 0, 3, 3]

        >>> get_sequence_number_2([0, 3, 6], 2020)
        436

        >>> get_sequence_number_2([3, 1, 2], 2020)
        1836

        >>> get_sequence_number_2([3, 2, 1], 2020)
        438

        >>> get_sequence_number_2([0, 13, 1, 8, 6, 15], 2020)
        1618
    """
    sequence_len = len(sequence)
    if position - 1 < sequence_len:
        return sequence[position - 1]

    spoken_numbers = {n: i + 1 for i, n in enumerate(sequence[:-1])}
    last_number = sequence[-1]
    new_number = last_number
    last_position = sequence_len
    for i in range(position - sequence_len):
        current_position = last_position + 1

        last_seen = spoken_numbers.get(last_number, None)
        spoken_numbers[new_number] = last_position

        if last_seen is None:
            new_number = 0
        else:
            new_number = last_position - last_seen

        last_number = new_number
        last_position = current_position

    return new_number


def main(args):
    with open(args.input_file) as input_data:
        sequence = list(map(int, input_data.read().split(',')))

    position = 2020
    number = get_sequence_number(sequence[:], position)
    print(f'The number at position {position} is {number}')

    position_2 = 30000000
    number_2 = get_sequence_number_2(sequence[:], position_2)
    print(f'The number at position {position_2} is {number_2}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 15: Rambunctious Recitation')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()

    main(args)
